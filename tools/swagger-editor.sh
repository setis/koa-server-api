#!/usr/bin/env bash
cd ../;
rm -rf resource/swagger-editor
rm swagger-editor.zip;
wget https://github.com/swagger-api/swagger-editor/releases/download/v2.9.9/swagger-editor.zip;
unzip swagger-editor.zip -d resource;
rm -rf resource/dist/config;
rm -rf resource/dist/spec-files;
mv resource/dist resource/swagger-editor
ln -s $PWD/resource/config/swagger/config $PWD/resource/swagger-editor/config;
ln -s $PWD/resource/config/swagger/spec-files $PWD/resource/swagger-editor/spec-files;
rm swagger-editor.zip;
