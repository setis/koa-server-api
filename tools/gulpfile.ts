import {dest, parallel, pipe, series, src, task, watch} from "gulp";
import del = require("del");
import tsc = require("gulp-typescript");

const tsProject = tsc.createProject("../tsconfig.json");


/**
 * Remove build directory.
 */
task("clean", () => {
    return del(["dist", "docs"]);
});
task("doc:dts", function () {
    let tsResult = src([
        "src/**/*.ts"
    ])
        .pipe(tsProject());
    return tsResult.dts
        .pipe(dest("docs"));
});
task("doc:clean", () => {
    return del(["docs"]);
});

task("doc", series("doc:clean", "doc:dts"));

task("compile", () => {
    let tsResult = src([
        "src/**/*.ts"
    ])
        .pipe(tsProject());
    return tsResult.js
        .pipe(dest("dist/"));
});
task("make", () => {
    return src([
        "src/**/*.json",
        "src/**/*.mustache"

    ])
        .pipe(dest("dist/"));
});
task("build:clean", () => {
    return del(["dist"]);
});
task("build",  series("build:clean", parallel("compile", "make")));

task("watch", () => {
    watch(["src/**/*.ts"], parallel("compile"));
    watch([
        "src/**/*.json",
        "src/**/*.mustache"

    ], parallel("make"));

});
task("build:watch", series("build", "watch"));

task("default", series("build", "doc", "watch"));

