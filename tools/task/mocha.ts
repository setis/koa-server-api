const gulp = require('gulp');
import  mocha = require('gulp-mocha');

gulp.task('test:spec', function () {
    return gulp.src('src/**/*.spec.ts')
        .pipe(mocha({
            reporter: 'nyan',
            require: ['ts-node/register']
        }));
});
gulp.task('test:func', function () {
    return gulp.src('src/test/*.ts')
        .pipe(mocha({
            reporter: 'nyan',
            require: ['ts-node/register']
        }));
});