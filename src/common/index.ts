import {createHash} from "crypto";
import {compare, genSaltSync, hash as bhash} from "bcrypt";
import {Context} from "koa";

export namespace inet {
    /**
     * @description перевод ip4 в число
     * @alias INET_ATON
     * @param {string} ip
     * @returns {number}
     */
    export function aton(ip: string): number {
        let dv = new DataView(new ArrayBuffer(4));
        ip
            .split(".")
            .forEach((value, index) => {
                dv.setUint8(index, Number(value));
            });
        return dv.getUint32(0);
    }

    /**
     * @description перевод число в ip4
     * @alias INET_NTOA
     * @param {number} num
     * @returns {string}
     */
    export function ntoa(num: number): string {
        let ndv = new DataView(new ArrayBuffer(4));
        ndv.setUint32(0, num);
        let a = new Array();
        for (let i = 0; i < 4; i++) {
            a[i] = ndv.getUint8(i);
        }
        return a.join(".");
    }

}

export namespace password {

    const saltRounds = 10;
    export const SALT = genSaltSync(saltRounds);

    /**
     * @description проверка пароля
     * @param {string} decrypt
     * @param {string} encrypt
     * @returns {Promise<boolean>}
     */
    export async function very(decrypt: string, encrypt: string) {
        return await compare(decrypt, encrypt);
    }

    /**
     * @description получение hash из пароля
     * @param {string} data
     * @param {string} salt
     * @returns {Promise<string>}
     */
    export async function hash(data: string, salt: string = SALT) {
        return await bhash(data, salt);
    }

}

export namespace uniq_user {
    /**
     * @description уникальный пользователь
     * @param {string} ip
     * @param {string} ua
     * @param {string} session
     * @returns {string}
     */
    export function create(ip: string, ua: string, session?: string, alg: string = "sha256") {
        let hash = createHash(alg).update(ua).digest();
        let net = inet.aton(ip);
        if (session) {
            return `${net}:${hash}:${hash.length}`;
        }
        return `${net}:${session}:${hash}:${hash.length}`;
    }

    /**
     *
     * @param {"ip" | "ip+ua" | "ip+ua+sid"} method
     * @param {string} sid
     * @returns {(ctx: Application.Context) => string}
     */
    export function method(method: "ip" | "ip+ua" | "ip+sid+ua", sid: string = "sid"): (ctx: Context) => string {
        switch (method) {
            case "ip":
                return (ctx: Context) => {
                    return ctx.ip;
                };
            case "ip+ua":
                return (ctx: Context) => {
                    return create(ctx.ip, ctx.request.header["user-agent"]);
                };
            case "ip+sid+ua":
                return (ctx: Context) => {
                    return create(ctx.ip, ctx.request.header["user-agent"], ctx.cookies.get(sid));
                };
        }

    }

}

export namespace uuid {
    /**
     *
     * @param {string} uuid
     * @returns {Buffer}
     */
    export function toBuffer(uuid: string): Buffer {
        if (!uuid) {
            return Buffer.alloc(16); // Return empty buffer
        }
        const hexStr = uuid.replace(/-/g, "");
        if (uuid.length !== 36 || hexStr.length !== 32) throw new Error(`Invalid UUID string: ${uuid}`);
        return Buffer.from(hexStr, "hex");
    }

    /**
     *
     * @param {Buffer} buffer
     * @returns {string}
     */
    export function toString(buffer: Buffer): string {
        if (buffer.length !== 16) throw new Error(`Invalid buffer length for uuid: ${buffer.length}`);
        if (buffer.equals(Buffer.alloc(16))) return ""; // If buffer is all zeros, return null
        const str = buffer.toString("hex");
        return `${str.slice(0, 8)}-${str.slice(8, 12)}-${str.slice(12, 16)}-${str.slice(16, 20)}-${str.slice(20)}`;
    }
}


export namespace response {
    export async function jsonSuccess(ctx: Context, next: () => Promise<void>, data: any, event?: { name: string, arguments?: any }) {
        ctx.body = JSON.stringify({
            ...(event) ? {
                event: event,
                user: (ctx.session && ctx.session.user) ? ctx.session.user : null
            } : {},
            ...{
                status: "success",
                success: data,
            }
        });
        ctx.type = "json";
        await next();
    }

    export async function jsonError(ctx: Context, next: () => Promise<void>, err: Error, event?: { name: string, arguments?: any }) {
        ctx.status = 500;
        ctx.body = JSON.stringify({
            ...(event) ? {
                event: event,
                user: (ctx.session && ctx.session.user) ? ctx.session.user : null
            } : {},
            ...{
                status: "error",
                error: {
                    msg: err.message,
                    name: err.name,
                    stack: err.stack
                },
            }
        });
        ctx.type = "json";
        await next();
    }
}
const emailExistence = require("email-existence");
export namespace checker {
    export function email(mail: string): Promise<boolean> {
        return new Promise(resolve => {
            emailExistence.check(mail, (error) => {
                resolve((error) ? false : true);
                if (error) {
                    console.log(`checker.email:${mail}`, error);
                }
            });
        });

    }
}