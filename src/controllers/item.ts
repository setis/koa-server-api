import {Context} from "koa";
import {getCustomRepository, getRepository} from "typeorm";
import {response} from "../common";
import * as Debug from "debug";
import {Category} from "../orm/entities/category";
import {CategoryTreeRepository} from "../orm/repositories/category";
import {Item} from "../orm/entities/item";

const debug = {
    log: Debug("controller:log:item"),
    info: Debug("controller:info:item"),
    error: Debug("controller:error:item"),
};
const CategoryRepository = getRepository(Category);
const ItemRepository = getRepository(Item);
const {jsonError, jsonSuccess} = response;

export default async function () {
    return {
        "item.list": category_list,
        "item.view": category_view,
        "item.create": item_add,
        "item.get": category_item,
        "item.update": category_update
    }
}

async function item_add(ctx: Context, next: () => Promise<void>) {
    let model = new Item();
    ["name", "description", "model"]
        .forEach(value => {
            model[value] = ctx.parameters[value];
        });
    let {category} = ctx.parameters;

    try {
        let categoryModel = await CategoryRepository.findOne(category);
        if (categoryModel === undefined) {
            throw new Error(`нет такой категории: ${category}`);
        }

        let d = await ItemRepository.createQueryBuilder(`t`)
            .select()
            .where(`t.name = :name OR t.model = :model`)
            .setParameters({
                name: model.name,
                model: model.model
            })
            .execute();
        debug.info(d);

        await jsonSuccess(ctx, next, d);
    } catch (e) {
        debug.error(e);
        await jsonError(ctx, next, e)
    }
}

async function category_tree(ctx: Context, next: () => Promise<void>) {
    const repo = getCustomRepository(CategoryTreeRepository);
    try {
        let data = await repo.tree();
        // ctx.set(`X-Response-Count`, data.length.toString());
        await jsonSuccess(ctx, next, data);

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }
}

async function category_list(ctx: Context, next: () => Promise<void>) {
    const repo = getRepository(Category);
    try {
        let [data, count] = await repo.findAndCount();
        debug.info(data, count);
        ctx.set(`X-Response-Count`, count.toString());
        await jsonSuccess(ctx, next, data);

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }
}

async function category_view(ctx: Context, next: () => Promise<void>) {
    const repo = getRepository(Category);
    try {
        let [data, count] = await repo.findAndCount({
            skip: ctx.parameters.offset,
            take: ctx.parameters.limit,
            order: {
                [ctx.parameters.sortBy]: (ctx.parameters.sortAsc) ? "ASC" : "DESC"
            },
        });
        debug.info(data, count);
        await jsonSuccess(ctx, next, {
            items: data,
            count,
            ...ctx.parameters
        });

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }
}

async function category_item(ctx: Context, next: () => Promise<void>) {
    const repo = getCustomRepository(CategoryTreeRepository);
    try {
        let data = await repo.parent(ctx.parameters.id);
        await jsonSuccess(ctx, next, data);

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }

}


async function category_update(ctx: Context, next: () => Promise<void>) {
    let {id, parent} = ctx.parameters;
    const repo = getRepository(Category);
    const repoTree = getCustomRepository(CategoryTreeRepository);
    try {
        let find = await repo.findOne(id);
        if (find === undefined) {
            throw new Error(`не найден id:${id}`);
        }
        ["name", "description"]
            .forEach(value => {
                find[value] = ctx.parameters[value];
            });
        if (parent != 0) {
            const p = await repo.findOneById(parent);
            if (p === undefined) {
                throw new Error(`не найден родительский категория:${parent}`);
            }
        }
        let result = await repo.save(find);
        await repoTree.set((parent != 0) ? parent : null, id);
        await jsonSuccess(ctx, next, result);
    } catch (e) {
        debug.error(e);
        await jsonError(ctx, next, e)
    }

}