import {Context} from "koa";
import {getRepository} from "typeorm";
import {checker, password as ps, response} from "../common";
import * as Debug from "debug";
import {PassportLocal} from "../orm/entities/passport-local";
import {User} from "../orm/entities/user";

const debug = {
    log: Debug("controller:log:user"),
    info: Debug("controller:info:user"),
    error: Debug("controller:error:user"),
};
const {jsonError, jsonSuccess} = response;
export default async function () {
    return {
        "user.register": user_register,
        "user.register.check": user_register_check,
        "user.login": user_login,
        "user.logout": user_logout,
        "user.islogin": user_is_login,
        "user.info": user_info,
    }
}

async function user_register_check(ctx: Context, next: () => Promise<void>) {
    let {method, data} = ctx.parameters;
    const PassportLocalRepository = getRepository(PassportLocal);
    switch (method) {
        case "email":
            try {
                let result: any = {
                    empty: undefined,
                    check: false
                };
                await Promise.all([
                    async () => {
                        result.empty = await PassportLocalRepository.findOne({where: {email: data}});
                    },
                    async () => {
                        result.check = await checker.email(data);
                    }
                ]);
                await jsonSuccess(ctx, next, (!result.check || result.empty !== undefined));
            } catch (e) {
                await jsonError(ctx, next, e);
            }
            break;
        case "phone":
            try {
                let result = await PassportLocalRepository.findOne({where: {phone: data}});
                await jsonSuccess(ctx, next, (result === undefined));
            } catch (e) {
                await jsonError(ctx, next, e);
            }
            break;
        case "username":
            try {
                let result = await PassportLocalRepository.findOne({where: {username: data}});
                await jsonSuccess(ctx, next, (result === undefined));
            } catch (e) {
                await jsonError(ctx, next, e);
            }
            break;
        default:
            await jsonError(ctx, next, new Error(`not found mehod:${method}`));
            return;
    }
}

async function user_register(ctx: Context, next: () => Promise<void>) {
    let {username, password, phone, email} = ctx.parameters;
    const PassportLocalRepository = getRepository(PassportLocal);
    let checker: any = {
        username: undefined,
        email: undefined,
        phone: undefined
    };
    checker.username = await PassportLocalRepository.findOne({
        where: {
            username: username
        },
        cache: false
    });
    checker.email = await PassportLocalRepository.findOne({
        where: {
            email: email
        },
        cache: false
    });
    checker.phone = await PassportLocalRepository.findOne({
        where: {
            phone: phone
        },
        cache: false
    });
    let errors = Object.keys(checker)
        .filter(value => {
            return checker[value] !== undefined;
        });
    if (errors.length > 0) {
        jsonError(ctx, next, new Error(`заняты ${errors.map(value => `${value}:${ctx.parameters[value]}`).join(" ")}`));
        return;
    }
    const UserRepository = getRepository(User);
    let user = new User();
    user.createAt = new Date();
    try {
        user = await UserRepository.save(user);
    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e);
        return;
    }

    let local = new PassportLocal();
    local.user = user;
    local.setPassword(password);

    local.email = email;
    local.phone = phone;
    local.username = username;

    try {
        await PassportLocalRepository.save(local);
        ctx.redirect("/");
    } catch (e) {
        debug.log(e);
        await UserRepository.remove(user);
        await jsonError(ctx, next, e);
        return;
    }
}

async function user_login(ctx: Context, next: () => Promise<void>) {
    const PassportLocalRepository = getRepository(PassportLocal);
    let {username, password, phone, email} = ctx.parameters;
    let encypt_password = await ps.hash(password);
    try {
        const result = await PassportLocalRepository
            .createQueryBuilder()
            .select()
            .from(PassportLocal, "l")
            .where("(l.username = :username AND l.password = :password) OR" +
                " (l.email = :email AND l.password = :password) OR" +
                " (l.phone = :phone AND l.password = :password)")
            .setParameters({
                email: email,
                username: username,
                phone: phone,
                password: encypt_password
            })
            .execute();
        if (result.userId) {
            ctx.status = 401;
            ctx.body = "";
            await next();
            return;
        }
        // const user = await getRepository(User).findOneById(result.userId);
        ctx.login(result.userId);
        ctx.redirect("/home")
    } catch (e) {
        await jsonError(ctx, next, e);
    }


}

async function user_logout(ctx: Context, next: () => Promise<void>) {
    if (ctx.isAuthenticated()) {
        ctx.logout();
    }
    ctx.redirect("/");
    await next();
}

async function user_is_login(ctx: Context, next: () => Promise<void>) {
    ctx.status = (ctx.isAuthenticated()) ? 201 : 401;
    ctx.body = "";
    await next();
}

async function user_info(ctx: Context, next: () => Promise<void>) {
    let data = {};
    data.isAuth = ctx.isAuthenticated();
    data.session = ctx.session;
    data.headers = ctx.headers;
    await jsonSuccess(ctx, next, data);
}