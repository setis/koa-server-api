import {Context} from "koa";
import {getCustomRepository, getRepository} from "typeorm";
import {response} from "../common";
import * as Debug from "debug";
import {Category} from "../orm/entities/category";
import {category_recovery} from "../events/category";
import {CategoryTreeRepository} from "../orm/repositories/category";

const debug = {
    log: Debug("controller:log:category"),
    info: Debug("controller:info:category"),
    error: Debug("controller:error:category"),
};
const {jsonError, jsonSuccess} = response;
export default async function () {
    return {
        "category.list": category_list,
        "category.view": category_view,
        "category.create": category_add,
        "category.tree": category_tree,
        "category.item": category_item,
        "category.update": category_update,
        "category.recovery": async (ctx: Context, next: () => Promise<void>) => {
            try {
                const result = await category_recovery();
                await jsonSuccess(ctx, next, result);
            } catch (e) {
                debug.log(e);
                await jsonError(ctx, next, e)
            }
        }
    }
}

async function category_add(ctx: Context, next: () => Promise<void>) {
    let model = new Category();
    ["name", "description"]
        .forEach(value => {
            model[value] = ctx.parameters[value];
        });
    let {parent} = ctx.parameters;
    const repo = getRepository(Category);
    const repoTree = getCustomRepository(CategoryTreeRepository);

    try {
        const find = await repo.findOne({name: model.name});
        if (find !== undefined) {
            throw new Error(`Duplicate name:${model.name}`);
        }
        if (parent != 0) {
            const p = await repo.findOne(parent);
            if (p === undefined) {
                throw new Error(`не найден родительский категория:${parent}`);
            }
        }

        const result = await repo.save(model);
        await repoTree.add((parent != 0) ? parent : null, result.id);
        await jsonSuccess(ctx, next, result);
    } catch (e) {
        debug.error(e);
        await jsonError(ctx, next, e)
    }
}

async function category_tree(ctx: Context, next: () => Promise<void>) {
    const repo = getCustomRepository(CategoryTreeRepository);
    try {
        let data = await repo.tree();
        // ctx.set(`X-Response-Count`, data.length.toString());
        await jsonSuccess(ctx, next, data);

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }
}

async function category_list(ctx: Context, next: () => Promise<void>) {
    const repo = getRepository(Category);
    try {
        let [data, count] = await repo.findAndCount();
        debug.info(data, count);
        ctx.set(`X-Response-Count`, count.toString());
        await jsonSuccess(ctx, next, data);

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }
}

async function category_view(ctx: Context, next: () => Promise<void>) {
    const repo = getRepository(Category);
    try {
        let [data, count] = await repo.findAndCount({
            skip: ctx.parameters.offset,
            take: ctx.parameters.limit,
            order: {
                [ctx.parameters.sortBy]: (ctx.parameters.sortAsc) ? "ASC" : "DESC"
            },
        });
        debug.info(data, count);
        await jsonSuccess(ctx, next, {
            items: data,
            count,
            ...ctx.parameters
        });

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }
}

async function category_item(ctx: Context, next: () => Promise<void>) {
    const repo = getCustomRepository(CategoryTreeRepository);
    try {
        let data = await repo.parent(ctx.parameters.id);
        await jsonSuccess(ctx, next, data);

    } catch (e) {
        debug.log(e);
        await jsonError(ctx, next, e)
    }

}


async function category_update(ctx: Context, next: () => Promise<void>) {
    let {id, parent} = ctx.parameters;
    const repo = getRepository(Category);
    const repoTree = getCustomRepository(CategoryTreeRepository);
    try {
        let find = await repo.findOne(id);
        if (find === undefined) {
            throw new Error(`не найден id:${id}`);
        }
        ["name", "description"]
            .forEach(value => {
                find[value] = ctx.parameters[value];
            });
        if (parent != 0) {
            const p = await repo.findOneById(parent);
            if (p === undefined) {
                throw new Error(`не найден родительский категория:${parent}`);
            }
        }
        let result = await repo.save(find);
        await repoTree.set((parent != 0) ? parent : null, id);
        await jsonSuccess(ctx, next, result);
    } catch (e) {
        debug.error(e);
        await jsonError(ctx, next, e)
    }

}