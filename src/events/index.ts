const db: Map<string, () => Promise<any>> = new Map();

export default db;