import events from "./index";
import {getConnection, getRepository, getTreeRepository} from "typeorm";
import {Category} from "../orm/entities/category";

interface CategoryRecoveryResult {
    ancestor: number,
    parent: number | null,
    descendant: number,
    id: number
}

events.set("category.recovery", category_recovery);

export async function category_recovery() {
    const result = await getConnection()
        .query("SELECT id FROM `category` WHERE `parentId` IS NULL")
    const repo = getTreeRepository(Category);
    let errors = [];
    let resource = await Promise
        .all(result
            .map(async (value: CategoryRecoveryResult) => {
                // if (value.ancestor != value.parent || value.descendant != value.id) {
                try {
                    const model = await repo.findOneById(value.id);
                    const parent = await repo.findDescendants(model);
                    console.log(model, parent);
                    return {model, parent}
                    // const parent = await repo.findOneById(value.parent);
                    // model.parent = parent;
                    // return await repo.save(model);
                } catch (e) {
                    errors.push({
                        value: value, error: {
                            msg: e.message,
                            name: e.name,
                            stack: e.stack
                        }
                    });
                    console.log(e);
                }

                // }
            })
        );
    resource = resource.filter(value => value !== undefined);
    return {
        status: {
            success: resource.length,
            error: errors.length,
        },
        resource: {
            success: resource,
            error: errors
        }
    }
}

export async function category_root(){
    getRepository(Category)
}