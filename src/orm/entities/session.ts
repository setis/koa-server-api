import {Column, Entity, ManyToOne} from "typeorm";
import {User} from "./user";

@Entity("session")
export default class Session {
    @Column({
        type: "varchar",
        length: 255,
        nullable: false,
        primary: true,
        unique: true
    })
    id: string;

    @ManyToOne(type => User, user => user.id)
    user: User;

    @Column({
        nullable: false,
        type: "varchar",
        length: 4

    })
    ip: string;

    @Column({
        nullable: false,
        type: "text"
    })
    ua: string;

    @Column({
        nullable: false,
        type: "timestamp"
    })
    login: Date;

    @Column({
        nullable: false,
        type: "timestamp"
    })
    logout: Date;

    @Column({
        nullable: false,
        type: "timestamp"
    })
    last_active: Date;


}