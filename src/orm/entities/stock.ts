import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Address} from "./address";
import {Item} from "./item";
import {User} from "./user";
import {DocumentGroup} from "./document";


/**
 * @description склад
 */
@Entity("stock")
export class Stock {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "varchar",
        length: 255,
        comment: "название склад"
    })
    name: string;

    @Column({
        type: "text",
        nullable: true,
        comment: "описание склад"
    })
    description: string | null;

    @Column({
        type: "int",
        length: 4,
        nullable: true,
        comment: " площадь в квадратных метрах"

    })
    square: number | null;

    @ManyToOne(type => Address, address => address.id)
    address: Address;

}


/**
 * @description склад с наименование
 */
@Entity("stock_item")
export class ItemStock {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Item, item => item.id)
    item: Item;

    @ManyToOne(type => User, user => user.id)
    user: User;

    @ManyToOne(type => Stock, stock => stock.id)
    stock: Stock;

    @Column({
        type: "int",
        nullable: false,
        comment: "количество"
    })
    count: number;

    @Column({
        type: "text",
        name: "sn",
        nullable: true,
        comment: "серийные номера"
    })
    sn: string | null;

    @OneToMany(type => DocumentGroup, document_group => document_group.id)
    document: DocumentGroup[];

}
