import {Column, Entity, EntityRepository, OneToMany, OneToOne, PrimaryGeneratedColumn, Repository} from 'typeorm'
import {PermissionUsers, RoleUsers} from "./acl";
import Session from "./session";
import {ContactUser} from "./contact";
import {PassportStrategy} from "./passport-strategy";
import {PassportLocal} from "./passport-local";

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({
        type: "timestamp"
    })
    createAt: Date;

    @OneToMany(type => PassportStrategy, passport => passport.user)
    passportStrategy: PassportStrategy[];
    @OneToOne(type => PassportLocal, passport => passport.user)
    passportLocal: PassportLocal;
    @OneToMany(type => RoleUsers, role => role.user)
    role: RoleUsers[];
    @OneToMany(type => PermissionUsers, permission => permission.user)
    permission: PermissionUsers[];
    @OneToMany(type => Session, session => session.user)
    session: Session[];
    @OneToMany(type => ContactUser, contact_user => contact_user.user)
    contact: ContactUser[];
}


@EntityRepository(User)
export class UserRepository extends Repository<User> {


}