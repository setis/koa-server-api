import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {createHash} from "crypto";
import {Item} from "./item";
import {User} from "./user";

@Entity("document")
export class Document {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "datetime",
        nullable: false,
        comment: "создание документа"
    })
    time: Date;

    @Column({
        type: "varchar",
        length: 255,
        comment: "название документа"
    })
    name: string;

    @Column({
        type: "text",
        nullable: true,
        comment: "описание документа"
    })
    description: string | null;

    @Column({
        type: "varchar",
        length: 255,
        nullable: false
    })
    type: string;

    @Column({
        type: "bigint",
        nullable: false
    })
    size: number;

    @Column({
        type: "varchar",
        length: 32,

    })
    hash: Buffer;

    @Column({
        type: "blob",
        nullable: false,
        name: "data"
    })
    private _data: Buffer;

    get data(): Buffer {
        return this._data;
    }

    set data(value: Buffer) {
        this._data = value;
        this.hash = createHash("sha256").update(this._data).digest();
        this.size = this._data.byteLength;
    }

}

@Entity("document_group")
export class DocumentGroup {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "datetime",
        nullable: false,
        comment: "создание документа"
    })
    time: Date;

    @Column({
        type: "varchar",
        length: 255,
        nullable: true,
        comment: "название документа"
    })
    name: string | null;

    @Column({
        type: "text",
        nullable: true,
        comment: "описание документа"
    })
    description: string | null;

}

@Entity("document_item")
export class DocumentItem {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.id)
    user: User;

    @ManyToOne(type => DocumentGroup, document_group => document_group.id)
    group: DocumentGroup;

    @ManyToOne(type => Item, item => item.id)
    item: Item;

}