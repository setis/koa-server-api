import {Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {Item} from "./item";
import {Contact} from "./contact";
import {Address} from "./address";
import {Image} from "./image";

/**
 * @description поставщик товара
 */
@Entity("provider")
export class Provider {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "varchar",
        length: 255,
        comment: "название поставщика"
    })
    name: string;

    @Column({
        type: "text",
        nullable: true,
        comment: "описание поставщика"
    })
    description: string | null;

    @Column({
        type: "varchar",
        length: 255,
        nullable: true,
        comment: "сайт"
    })
    site: string | null;

    @OneToMany(type => ContactProvider, contact_provider => contact_provider.provider)
    contact: ContactProvider[];

    @OneToMany(type => AddressProvider, address_provider => address_provider.address)
    address: AddressProvider[];

    @OneToMany(type => LogotypeProvider, logotype_provider => logotype_provider.logotype)
    logotype: LogotypeProvider[];

    @OneToMany(type => ItemProvider, item => item.provider)
    item: ItemProvider[];

}

/**
 * @description поставщик в какие страны
 */
@Entity("provider_counter")
export class ProviderCounter {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "char",
        length: 2,
        nullable: false
    })
    counter: string;

    @ManyToOne(type => Provider, provider => provider.id)
    provider: Provider;

}

@Entity("provider_contact")
export class ContactProvider {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Provider, provider => provider.id)
    provider: Provider;

    @ManyToOne(type => Contact, contact => contact.id)
    contact: Contact;

}

@Entity("provider_address")
export class AddressProvider {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Provider, provider => provider.id)
    provider: Provider;

    @ManyToOne(type => Address, address => address.id)
    address: Address;

}

@Entity("provider_logotype")
export class LogotypeProvider {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Provider, provider => provider.id)
    provider: Provider;

    @ManyToOne(type => Image, image => image.id)
    logotype: Image;

    @Column({
        type: Boolean,
        default: false,
        nullable: false,
        comment: "основная"
    })
    main: boolean;
}

/**
 * @description магазин (поставщик,цена,товар,количество)
 */
@Entity("provider_item")
export class ItemProvider {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Item, item => item.id)
    item: Item;

    @ManyToOne(type => Provider, provider => provider.id)
    provider: Provider;

    @Column({
        type: "int",
        nullable: false,
        comment: "количество"
    })
    count: number;

    @Column({
        type: "float",
        nullable: false,
        comment: "цена"
    })
    price: number;

    @Column({
        type: "char",
        length: 4,
        nullable: false,
        comment: "валюта"
    })
    currency: string;

    @Column({
        type: Boolean,
        default: false,
        nullable: false,
        comment: "выстовить на продажу"
    })
    online: boolean;

}

