import {
    ClosureEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn, TreeChildren, TreeLevelColumn,
    TreeParent
} from "typeorm";
import {User} from "./user";

@Entity("company")
export class Company {

    @PrimaryGeneratedColumn()
    id: number;


    name: string;

    @ManyToOne(type => User, user => user.id)
    master: Promise<User>;


}

@ClosureEntity("company_position")
export class CompanyPosition {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.id)
    user: Promise<User>;

    @Column({
        nullable: false,
        unique: true
    })
    name: string;

    @TreeChildren({lazy: true})
    children: CompanyPosition;

    @TreeParent({lazy: true})
    parent: CompanyPosition;

    @TreeLevelColumn()
    level: number;
}

@Entity("company_employees")
export class CompanyEmployee {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Company, company => company.id)
    company: Promise<Company>;

    @ManyToOne(type => User, user => user.id)
    user: Promise<User>;

    @ManyToOne(type => User, user => user.id)
    position: CompanyPosition;


}