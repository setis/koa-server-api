import {Column, Entity, ManyToOne, PrimaryGeneratedColumn} from "typeorm"
import {User} from "./user";

@Entity("passport_strategy")
export class PassportStrategy {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    profile_id: number;
    @Column()
    strategy: string;
    @Column()
    accessToken: string;
    @Column({
        nullable: true,
        type: "varchar",
        length: 255,
        default: null
    })
    refreshToken: string | null;
    @ManyToOne(type => User, user => user.id)
    user: User;

    @Column({
        name: "profile",
        type: "text",
        nullable: false,
    })
    private _profile: string;

    get profile(): any {

        return JSON.parse(this._profile);
    }

    set profile(value: any) {
        this._profile = JSON.stringify(value);


    }
}