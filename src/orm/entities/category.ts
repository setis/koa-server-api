import {
    Column, Entity, Index, JoinColumn, ManyToMany, ManyToOne, OneToMany, OneToOne,
    PrimaryGeneratedColumn
} from "typeorm";
import {Image} from "./image";
import {User} from "./user";
import {Property} from "./property";
import {Item} from "./item";
//
//
// @ClosureEntity("category")
// export class Category {
//
//     @PrimaryGeneratedColumn()
//     id: number;
//
//     @Column({
//         nullable: false,
//         unique: true,
//         collation: "utf8_general_ci"
//     })
//     name: string;
//
//     @Column({
//         nullable: true,
//         type: "text",
//         collation: "utf8_general_ci",
//         default: null
//     })
//     description: string | null;
//
//
//     @TreeParent({lazy: true})
//     parent: Category;
//
//
//     @TreeChildren({cascadeUpdate: true, cascadeInsert: true, lazy: true})
//     children: Category[];
//
//     @TreeLevelColumn()
//     level: number;
//
//     //
//     // @ManyToMany(type => CategoryImage, categoty_image => categoty_image.category)
//     // images: Promise<CategoryImage[]>;
//     //
//     // @ManyToMany(type => Item, item => item.category)
//     // items: Promise<Item[]>;
//     //
//     // @ManyToMany(type => CategoryProperty, category_property => category_property.category)
//     // properties: Promise<CategoryProperty[]>;
//
//
// }


@Entity("category_data")
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        nullable: false,
        unique: true,
        collation: "utf8_general_ci"
    })
    name: string;

    @Column({
        nullable: true,
        type: "text",
        collation: "utf8_general_ci",
        default: null
    })
    description: string | null;
    //
    // @ManyToOne(type => CategoryTree, tree => tree.ancestor)
    // ancestor: CategoryTree;
    //
    // @OneToMany(type => CategoryTree, tree => tree.descendant)
    // descendant: CategoryTree[];
    //


    @OneToOne(type => CategoryTree, tree => tree.descendant)
    parent: CategoryTree = null;

    @OneToMany(type => CategoryTree, tree => tree.ancestor)
    children: CategoryTree[];
    //
    // @ManyToMany(type => CategoryImage, categoty_image => categoty_image.category)
    // images: Promise<CategoryImage[]>;
    //
    // @ManyToMany(type => Item, item => item.category)
    // items: Promise<Item[]>;
    //
    // @ManyToMany(type => CategoryProperty, category_property => category_property.category)
    // properties: Promise<CategoryProperty[]>;


}

@Entity("category_tree")
@Index("level", ["level"])
export class CategoryTree {

    @Column({
        primary: true,
        type: "varchar",
        length: 255,
        nullable: false,
        unique: true

    })
    id: string;


    @Column({
        nullable: true,
        type: "int",
    })
    ancestorId: number | null;

    @ManyToOne(type => Category, category => category.id, {
        cascadeUpdate: true,
        nullable: true,
        lazy: true
    })
    @JoinColumn()
    // @JoinColumn({name: "ancestorId", referencedColumnName: "id"})
    ancestor: Category;

    @Column({
        nullable: false,
        type: "int",
    })
    descendantId: number;

    @ManyToOne(type => Category, category => category.id, {
        cascadeUpdate: true,
        nullable: false,
        lazy: true,
    })
    @JoinColumn()
    // @JoinColumn({name: "descendantId", referencedColumnName: "id"})
    descendant: Category[];


    @Column({
        default: 0,
        nullable: false
    })
    level: number = 0;

}

@Entity("category_image")
export class CategoryImage {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Image, image => image.id)
    image: Promise<Image>;

    @ManyToOne(type => Category, category => category.id)
    category: Promise<Category>;

    @Column({
        type: "timestamp",
        nullable: false
    })
    time: Date;

    @Column({
        type: Boolean,
        nullable: false,
        default: true
    })
    display: boolean;

    @ManyToOne(type => User, user => user.id)
    user: Promise<User | null>;
}

@Entity("category_property")
export class CategoryProperty {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Category, category => category.id)
    category: Promise<Category>;

    @ManyToOne(type => Property, property => property.id)
    property: Promise<Property>;
}

//
// @EntityRepository(CategoryImage)
// export class CategoryImageRepository extends Repository<CategoryImage> {
//     /**
//      * @description загрузка изображение на категории
//      * @param {Buffer} image
//      * @param {Category | number} category
//      * @returns {Promise<CategoryImage>}
//      */
//     async upload(image: Buffer, category: Category | number): Promise<CategoryImage> {
//         let categoryModel;
//         if (typeof category === "number") {
//             let model = await getRepository(Category).findOneById(category);
//             if (model instanceof Category) {
//                 categoryModel = model;
//             } else {
//                 throw new Error(` не найден категория category.id:${category}`);
//             }
//         } else {
//             categoryModel = category;
//         }
//
//         const ImageRepository = getRepository(Image);
//         let time = new Date();
//         let img = new Image();
//         img.image = image;
//         img.name = categoryModel.name;
//         img.time = time;
//         img.original = null;
//         img = await ImageRepository.save(img);
//         let imgCategory = new CategoryImage();
//         imgCategory.category = Promise.resolve(categoryModel);
//         imgCategory.image = Promise.resolve(img);
//         imgCategory.user = Promise.resolve(null);
//         imgCategory.display = true;
//         return await this.save(imgCategory);
//
//     }
//
//     /**
//      * @description добавить изображение к категории
//      * @param {Image | number} image
//      * @param {Category | number} category
//      * @returns {Promise<CategoryImage>}
//      */
//     async addImage(image: Image | number, category: Category | number): Promise<CategoryImage> {
//         let categoryModel;
//         if (typeof category === "number") {
//             let model = await getRepository(Category).findOneById(category);
//             if (model instanceof Category) {
//                 categoryModel = model;
//             } else {
//                 throw new Error(` не найден категория category.id:${category}`);
//             }
//         } else {
//             categoryModel = category
//         }
//
//         let imgCategory = new CategoryImage();
//         if (image instanceof Image) {
//             imgCategory.image = Promise.resolve(image);
//         } else {
//             let id = getRepository(Image).findOneById(image);
//             if (id instanceof Image) {
//                 imgCategory.image = Promise.resolve(id);
//             } else {
//                 throw new Error(`не найден изображение image.id:${image}`);
//             }
//         }
//         imgCategory.category = Promise.resolve(categoryModel);
//         imgCategory.user = Promise.resolve(null);
//         imgCategory.display = true;
//         return await this.save(imgCategory);
//     }
//
//     async removeImage(image: Image | number, category: Category | number): Promise<void> {
//         let categoryModel;
//         if (typeof Category === "number") {
//             let model = await getRepository(Category).findOneById(category);
//             if (model instanceof Category) {
//                 categoryModel = model;
//             } else {
//                 throw new Error(` не найден категория category.id:${category}`);
//             }
//         } else {
//             categoryModel = category
//         }
//         let imageModel;
//         const ImageRepository = getRepository(Image);
//         if (image instanceof Image) {
//             imageModel = image;
//         } else {
//             let id = ImageRepository.findOneById(image);
//             if (id instanceof Image) {
//                 imageModel = id;
//             } else {
//                 throw new Error(`не найден изображение image.id:${image}`);
//             }
//         }
//         await ImageRepository.delete(imageModel);
//     }
//
//     download() {
//
//     }
//
//
// }
//
// @EntityRepository(CategoryTree)
// export class CategoryTreeRepository extends Repository<Category> {
//
//     async level(id: number): Promise<Number> {
//         const data = await getRepository(Category)
//             .createQueryBuilder("data")
//             .leftJoinAndSelect("data.descendant", "tree")
//             .where("tree.ancestor = :id")
//             .setParameter("id", id)
//             .execute();
//         return data.length + 1;
//     }
// }
//
// @EntityRepository(Category)
// export class CategoryRepository extends Repository<Category> {
//
//     /**
//      * @description Выборка потомков
//      * @param {number} id
//      * @returns {Promise<any>}
//      * @constructor
//      */
//     async selectDescendants(id: number) {
//         return await this.createQueryBuilder("data")
//             .leftJoinAndSelect("data.descendant", "tree")
//             .where("tree.ancestor = :id")
//             .setParameter("id", id)
//             .execute();
//     }
//
//     /**
//      * @description Выборка предков
//      * @param {number} id
//      * @returns {Promise<any>}
//      * @constructor
//      */
//     async selecAncestors(id: number) {
//         return await this.createQueryBuilder("data")
//             .leftJoinAndSelect("data.ancestor", "tree")
//             .where("tree.descendant = :id")
//             .setParameter("id", id)
//             .execute();
//     }
//
//     /**
//      * @description Удаление элемента
//      * @param {number} id
//      * @returns {Promise<void>}
//      */
//     async removeElement(id: number) {
//         const CategoryTreeRepository = getRepository(CategoryTree);
//
//         const data = await this.findOneById(id);
//         const tree = await CategoryTreeRepository.find({where: {descendant: id}});
//
//         await super.remove(data);
//         await CategoryTreeRepository.remove(tree);
//     }
//
//     /**
//      * @description Удаление вложенного дерева
//      * @param {number} id
//      * @returns {Promise<void>}
//      */
//     async removeAttachment(id: number) {
//
//         await this.createQueryBuilder("category")
//             .delete()
//             .where(
//                 qb => {
//                     return "category.id IN " + qb.subQuery()
//                         .select("descendant")
//                         .from(qb => {
//                             qb.subQuery()
//                                 .select("descendant")
//                                 .from(Category)
//                                 .leftJoinAndSelect("tree.descendant", "category")
//                                 .where("ancestor = :id")
//                                 .getQuery()
//                         })
//                         .getQuery();
//                 })
//             .setParameter("id", id)
//             .execute();
//         await getRepository(CategoryTree)
//             .createQueryBuilder("category_tree")
//             .delete()
//             .where(
//                 qb => {
//                     return "category_tree.id IN " + qb.subQuery()
//                         .select("descendant")
//                         .from(qb => {
//                             return qb.subQuery()
//                                 .select("descendant")
//                                 .from(CategoryTree)
//                                 .where("ancestor = :id")
//                                 .getQuery()
//                         })
//                         .getQuery();
//                 })
//             .setParameter("id", id)
//             .execute();
//
//     }
//
//     // /**
//     //  * @description создать категорию указав родительскую категорию и изображение на категорию
//     //  * @param {{name: string; parent: Category | number; description: string | null; image: Buffer | Image | number | null}} parameters
//     //  * @returns {Promise<Category>}
//     //  */
//     // async add(parameters: { name: string, parent: Category | number, description: string | null, image: Buffer | Image | number | null }): Promise<Category> {
//     //     let {name, parent, description = null, image = null} = parameters;
//     //     const CategoryImage = getCustomRepository(CategoryImageRepository);
//     //     let category = new Category();
//     //     category.name = name;
//     //     category.description = description;
//     //     if (parent instanceof Category || parent === null) {
//     //         category.parent = parent;
//     //     }
//     //     else {
//     //         let id = this.findOneById(parent);
//     //         if (id instanceof Category) {
//     //             category.parent = id;
//     //         }
//     //         else {
//     //             throw new Error(`не найден родительский категория category.id:${parent}`);
//     //         }
//     //
//     //     }
//     //     let result = await this.save(category);
//     //     if (image !== null) {
//     //         if (image instanceof Buffer) {
//     //             await CategoryImage.upload(image, category);
//     //
//     //         }
//     //         else if (image instanceof Image || typeof image === "number") {
//     //             await CategoryImage.addImage(image, category);
//     //         }
//     //
//     //     }
//     //     return result;
//     // }
//     //
//     // /**
//     //  * @description все зависимости категории
//     //  * @param {number | Category} category
//     //  * @returns {Promise<{children: Category[]; image: CategoryImage[]; item: Item[]; property: CategoryProperty[]}>}
//     //  */
//     // async depends(category: number | Category): Promise<{ children: Category[], image: CategoryImage[], item: Item[], property: CategoryProperty[] }> {
//     //     let modelCategory: Category;
//     //     if (category instanceof Category) {
//     //         modelCategory = category;
//     //     } else {
//     //         let id = this.findOneById(category);
//     //         if (id instanceof Category) {
//     //             modelCategory = id;
//     //         } else {
//     //             throw new Error(`не найдена категория category.id:${category}`);
//     //         }
//     //     }
//     //     let childerns = await modelCategory.children;
//     //     let images = await modelCategory.images;
//     //     let items = await modelCategory.items;
//     //     let properties = await modelCategory.properties;
//     //     return {
//     //         children: childerns,
//     //         image: images,
//     //         item: items,
//     //         property: properties
//     //     };
//     // }
//     //
//     // async remove(category: number | Category) {
//     //     let modelCategory: Category;
//     //     if (category instanceof Category) {
//     //         modelCategory = category;
//     //     } else {
//     //         let id = this.findOneById(category);
//     //         if (id instanceof Category) {
//     //             modelCategory = id;
//     //         } else {
//     //             throw new Error(`не найдена категория category.id:${category}`);
//     //         }
//     //     }
//     //     return await super.remove(modelCategory);
//     // }
//     //
//     // async list(offset: number = 0, limit = 0, order: { [field: string]: "ASC" | "DESC" } = {id: "ASC"}) {
//     //     return await this.findAndCount({
//     //         relations: ["category_image"],
//     //         skip: offset,
//     //         take: limit,
//     //         order: order,
//     //     });
//     // }
//
// }
//
