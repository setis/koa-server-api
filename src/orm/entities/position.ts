import {
    ClosureEntity,
    Column,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    TreeChildren,
    TreeLevelColumn,
    TreeParent
} from "typeorm";
import {User} from "./user";
import {Role} from "./acl";

@ClosureEntity("position")
export class Position {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => User, user => user.id)
    user: Promise<User>;

    @Column({
        nullable: false,
        unique: true
    })
    name: string;

    @TreeChildren({lazy: true})
    children: Position;

    @TreeParent({lazy: true})
    parent: Position;

    @TreeLevelColumn()
    level: number;
}

@Entity("position_role")
export class PositionRole {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(type => Role, role => role.id)
    role: Promise<Role>;

    @ManyToOne(type => Position, position => position.id)
    position: Promise<Position>;
}