import {Column, PrimaryGeneratedColumn} from "typeorm";
import {createHash} from 'crypto';

export class Link {

    @PrimaryGeneratedColumn("uuid")
    id: string;
    @Column({
        type: "timestamp",
        default: "Now()",
        nullable: false,
    })
    ctime: Date;

    @Column({
        type: "timestamp",
        nullable: false,
    })
    /**
     * @todo поставить в планировщик на очистку ссылок исчерпавшие время жизни
     */
    expires: Date;
    @Column({
        nullable: false,
    })
    event: string;


    @Column({
        type: 'binary',
        length: 32
    })
    hash: Buffer;
    @Column({
        nullable: false,
        name: "parameters"
    })

    private _parameters: string;

    get parameters() {
        return JSON.parse(this._parameters);
    }

    set parameters(value: string) {
        this._parameters = JSON.stringify(value);
        this.hash = createHash('sha256').update(this._parameters).digest();
    }
}