import {EntityRepository, getRepository, Repository} from "typeorm";
import {Category, CategoryTree} from "../entities/category";

export interface InterfaceCategoryTreeRepository {
    add(ancestor: Category | null, descendant: Category): Promise<void>;

    add(ancestor: number | null, descendant: number): Promise<void>;

    move(entity: Category | null, target: Category): Promise<void>;

    move(entity: number | null, target: number): Promise<void>;

    set(entity: Category | null, target: Category): Promise<void>;

    set(entity: number | null, target: number): Promise<void>;

    tree(id: number): Promise<Category>;

    tree(): Promise<Category[]>;
}

@EntityRepository(CategoryTree)
export class CategoryTreeRepository extends Repository<CategoryTree> implements InterfaceCategoryTreeRepository {

    async add(ancestor, descendant) {

        if ((ancestor instanceof Category || ancestor === null) && descendant instanceof Category) {
            if (ancestor.id === descendant.id) {
                throw new Error(`категория родительская не может быть что и дочернья`)
            }
            let tree = new CategoryTree();
            tree.ancestor = ancestor;
            tree.descendant = [descendant];
            tree.id = `${ancestor.id}:${descendant.id}`;
            return await this.save(tree);
        }
        else if ((typeof ancestor === "number" || ancestor === null) && typeof descendant === "number") {
            if (ancestor === descendant) {
                throw new Error(`категория родительская не может быть что и дочернья`)
            }
            console.log(await this.createQueryBuilder()
                .insert()
                .into(CategoryTree)
                .values([
                    {
                        ancestorId: ancestor,
                        descendantId: descendant,
                        id: `${ancestor}:${descendant}`
                    }
                ]).getQuery())
            return await this.createQueryBuilder()
                .insert()
                .into(CategoryTree)
                .values([
                    {
                        ancestorId: ancestor,
                        descendantId: descendant,
                        id: `${ancestor}:${descendant}`
                    }
                ])
                .execute();
        }
        else {
            throw new Error(`не правильный ввод аргументов`);
        }

    }

    async move(entity, target) {
        if ((entity instanceof Category || entity === null) && target instanceof Category) {
            if (entity.id === target.id) {
                throw new Error(`категория родительская не может быть что и дочернья`)
            }
            const parent = await this.findOne({
                where: {
                    descendantId: entity.id
                }
            });
            if (parent) {
                await this.remove(parent);
            }
            let tree = new CategoryTree();
            tree.ancestor = target;
            tree.descendant = entity;
            tree.id = `${target.id}:${entity.id}`;
            return await this.save(tree);
        }
        else if ((typeof entity === "number" || entity === null) && typeof target === "number") {
            if (entity === target) {
                throw new Error(`категория родительская не может быть что и дочернья`)
            }
            return await this.createQueryBuilder()
                .delete()
                .from(CategoryTree)
                .where({
                    descendantId: entity
                })
                .insert()
                .into(CategoryTree)
                .values([
                    {
                        ancestorId: target,
                        descendantId: entity,
                        id: `${target}:${entity}`
                    }
                ])
                .execute();
        }
        else {
            throw new Error(`не правильный ввод аргументов`);
        }
    }

    async set(entity, target) {
        try {
            this.add(target, entity);
        } catch (e) {
            if (e.code && e.code === "ER_DUP_ENTRY") {
                this.move(entity, target);
                return;
            }
            throw e;
        }
    }

    /**
     * @description Выборка потомков
     * @param {number} id
     * @returns {Promise<any>}
     * @constructor
     */
    async descendants(id: number): Promise<number[]> {
        return await this.createQueryBuilder("data")
            .leftJoinAndSelect("data.descendant", "tree")
            .where("tree.ancestor = :id")
            .setParameter("id", id)
            .execute();
    }

    /**
     * @description Выборка предков
     * @param {number} id
     * @returns {Promise<any>}
     * @constructor
     */
    async ancestors(id: number): Promise<number[]> {
        return await this.createQueryBuilder("data")
            .select("data.id")
            .leftJoinAndSelect("data.ancestor", "tree")
            .where("tree.descendant = :id")
            .setParameter("id", id)
            .execute();
    }

    async level(id: number): Promise<number> {
        const data = await getRepository(Category)
            .createQueryBuilder("data")
            .leftJoinAndSelect("data.ancestor", "tree")
            .where("tree.descendantId = :id")
            .orderBy("data.id")
            .setParameter("id", id)
            .execute();
        return data.length;
    }

    async relevel(size: number = 10) {
        const arr = await getRepository(Category).find();
        while (arr.length > 0) {
            let chunk = (arr.length < size) ? arr.splice(0, arr.length) : arr.splice(0, size);
            await Promise.all(chunk
                .map(async (model: Category) => {
                    let level, tree;
                    try {
                        level = await this.level(model.id) || 1;
                        tree = await this.findOne({
                            where: {
                                ancestor: model.id
                            }
                        });
                        tree.level = level;
                        await this.save(tree);
                    } catch (e) {
                        console.log(model, level, tree, e);
                    }
                }));
        }

    }

    async has(id: number): Promise<number> {
        const result = await this.createQueryBuilder()
            .select("COUNT(*)", "count")
            .from(CategoryTree, "tree")
            .where("tree.descendantId = :id")
            .setParameter("id", id)
            .execute();
        return result.count;

    }

    async root(): Promise<number[]> {
        const result = await this.createQueryBuilder()
            .select("descendantId")
            .where(`ancestorId IS NULL`)
            .execute();
        return (result === undefined) ? [] : result.map(value => value.descendantId);
    }

    async tree(id) {
        const repository = getRepository(Category);
        if (id) {
            const result = await repository.findOne({where: {id: id}, relations: ["children"]});
            let children = await Promise
                .all(result
                    .children
                    .map(async (value) => {
                        return await this.tree(value.descendantId);
                    }));
            result.children = children;
            return result;
        }
        let root = await this.root();
        return await Promise
            .all(
                root
                    .map(async (id) => {
                        return await this.tree(id);
                    })
            );
    }

    async parent(id: number, model: boolean = true) {
        const repository = getRepository(Category);
        let data = await repository.findOne({
            where: {id: id},
            relations: ["parent"]
        });
        data.parent = (model && data.parent !== null) ? await repository.findOne({where: {id: data.parent.ancestorId}}) : data.parent;
        return data;
    }
}
