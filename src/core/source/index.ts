import {SourceCore} from "./core";

let instance: SourceCore;

export function getInstanceSource() {
    if (instance === undefined) {
        instance = new SourceCore();
    }
    return instance;
}
export default getInstanceSource;