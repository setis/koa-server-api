import {RedisOptions} from "ioredis";
import {config} from "../config";

export class SourceCore {

    resource: Map<string, any> = new Map();
    config: Map<string, {
        use: "ioredis";
        config: RedisOptions;
    } | {
        use: string;
        config: any;
    }> = new Map();
    close: Map<string, () => void> = new Map();

    constructor(cfg: {
        [name: string]: {
            use: "ioredis";
            config: RedisOptions;
        } | {
            use: string;
            config: any;
        }
    } = config.source) {
        process.on("exit", async () => {
            await Promise
                .all(
                    Array.from(this.close.values())
                );
        });
        Object.keys(cfg).forEach(value => {
            this.config.set(value, cfg[value]);
        })
    }

    has(name: string) {
        if (!this.config.has(name)) {
            return false;
        }
        let {use} = this.config.get(name);
        try {
            let mod = require(`${__dirname}/source/${use}`);
            if (typeof mod.default === "function") {
                return true;
            }
        } catch (e) {
            console.error(e);
        }
        return false;
    }

    async use(name: string) {
        if (!this.resource.has(name)) {
            if (!this.config.has(name)) {
                throw new Error(`нету такого источника:${name}`);
            }
            return await this.source(name);
        }
        return this.resource.get(name);
    }

    async source(name: string, cfg: any = this.config.get(name)): Promise<any> {
        let {use, config} = cfg;
        let bootstrap = require(`${__dirname}/source/${use}`).default;
        let [source, close] = await bootstrap(this, config);
        this.resource.set(name, source);
        this.close.set(name, close);
        return source;

    }

}