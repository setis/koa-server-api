import * as RedisStorage from "ioredis";
import {RedisOptions} from "ioredis";
import {SourceCore} from "../core";

export default function (core: SourceCore, config: RedisOptions) {
    let source = new RedisStorage(config);
    // await source.connect();
    return Promise.resolve([source, () => {
        source.disconnect();
        return Promise.resolve();
    }]);
}