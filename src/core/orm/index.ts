import "reflect-metadata";
import {Connection, ConnectionOptions, createConnections} from "typeorm";
import index from "../config/index";
import {join} from "path";
import {Environment} from "../env";

let connections: Connection[] = [];
const ListDir = ["entities", "migrations", "subscribers"];

export default loader;

export async function loader(): Promise<Connection[]> {
    let options: ConnectionOptions[] = [];
    for (let name in index.typeorm) {
        let cfg = {...index.typeorm[name], name: name};
        ListDir.forEach(value => {
            cfg[value] = (cfg[value] instanceof Array) ?
                cfg[value].map(value => {
                    return join(Environment.DIR.ORM, value);
                }) : [
                    join(Environment.DIR.ORM, `/${value}/*{.js,.ts}`)
                ];
        });
        options.push(cfg);
    }
    return connections = await createConnections(options);
}

export function unloader() {
    connections.forEach(connection => {
        connection.isConnected && connection.close();
    });
}

process.on("exit", unloader);