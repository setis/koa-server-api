export class HttpError extends Error {
    code: number;
    event?: {
        name: string,
        args: any
    };

    constructor(code: number = 500, message?: string, event?: {
        name: string,
        args: any
    }) {
        super(message);
        this.code = code;
        this.event = event;
        // Error.captureStackTrace(this, HttpError);

    }

    /**
     *
     * @param {Error} err
     * @param {{name: string; args: any}} event
     * @returns {HttpError}
     * @constructor
     */
    static Error(err: Error, event?: {
        name: string,
        args: any
    }): HttpError {
        let error = new HttpError(500, err.message, event);
        error.stack = err.stack;
        return error;
    }


    toObject() {
        return {
            ...{
                status: "error",
                error: {
                    name: this.name,
                    msg: this.message,
                    stack: this.stack
                }
            },
            ...(this.event) ? this.event : {}

        };
    }

    toJson(): string {
        return JSON.stringify(this.toObject());
    }
}