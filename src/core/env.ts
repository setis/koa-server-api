import {join, normalize} from "path";
import moment = require("moment");

export enum EnvType {
    PRODUCTION,
    DEVELOPMENT,
    TEST
}

function env(): EnvType {
    switch (process.env.NODE_ENV) {
        default:
        case "production":
            return EnvType.PRODUCTION;
        case "development":
            return EnvType.DEVELOPMENT;
        case "test":
            return EnvType.TEST;
    }
}

export namespace Environment {
    export namespace DIR {
        export const ROOT = normalize(join(__dirname, `../../`));

        export const RESOURCE = join(ROOT, "resource");
        export const PUBLIC = join(ROOT, "public");

        export const CACHE = join(RESOURCE, "cache");
        export const CONFIG = join(RESOURCE, "config");
        export const LOG = join(RESOURCE, "log");

        export const SOURCE = join(ROOT, "src");
        export const KERNEL = join(SOURCE, "core");
        export const ORM = join(SOURCE, "orm");
        export const CONTROLLER = join(SOURCE, "controllers");
        export const SESSION = join(KERNEL, "session/store");
        export const MIDDLEWARE = join(SOURCE, "middlewares");
        export const PLUGIN = join(SOURCE, "plugins");

    }
    export const Env = env();

    export function isDev(): boolean {
        return Env === EnvType.DEVELOPMENT;
    }

    export function isTest(): boolean {
        return Env === EnvType.TEST;
    }

    export function isProduction(): boolean {
        return Env === EnvType.PRODUCTION;
    }

    export function Name(): string {
        switch (Env) {
            case EnvType.TEST:
                return "test";
            case EnvType.PRODUCTION:
                return "production";
            case EnvType.DEVELOPMENT:
                return "development";
        }
    }

    export const StartTime = new Date();
    export const StartTimeStamp = StartTime.getTime();


    export function UpStartStramp(): number {
        return new Date().getTime() - StartTimeStamp;
    }

    export function UpStartTime(): string {
        return moment(StartTime).startOf("day").fromNow();
    }


}