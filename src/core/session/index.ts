import {Context} from "koa";
import {randomBytes} from "crypto";
import * as Debug from "debug";

const debug = {
    log: Debug("kernel:mid:log"),
    info: Debug("kernel:session:info"),
    error: Debug("kernel:session:error"),
    debug: Debug("kernel:session:debug"),
};

export interface InterfaceSessionStore<T> {

    get(sid: string): Promise<undefined | T>;

    set(sid: string, session: T, exp: number): Promise<void>;

    destroy(sid: string): Promise<void>;
}

export interface InterfaceSessionCookie {
    maxAge: number,
    expires: number,
    path: string,
    domain?: string,
    secure: boolean,
    httpOnly: boolean,
    signed: boolean,
    overwrite: boolean,
}

export class SessionContext<E, D> {
    ctx: Context;
    session: D | any;
    store: InterfaceSessionStore<E>;
    key: string;
    options: InterfaceSessionCookie;
    id: string;
    send: boolean;

    constructor(ctx: Context, key: string, options: InterfaceSessionCookie, store: InterfaceSessionStore<E>, generate?: () => string) {
        this.ctx = ctx;
        this.options = options;
        this.key = key;
        this.store = store;
        this.send = false;
        this.session = {};
        if (typeof generate === "function") {
            this.generate = generate;
        }
    }

    generate(): string {
        return randomBytes(24).toString("hex");
    }

    encode(data: D): string {
        return JSON.stringify(data);
    }

    decode(data: string): E {
        return JSON.parse(data);
    }

    create_id() {
        this.id = this.generate();
        this.setId();
    }

    setId() {
        if (!this.send) {
            this.ctx.cookies.set(this.key, this.id, this.options);
            this.send = true;
        }

    }

    getId() {
        this.id = this.ctx.cookies.get(this.key, this.options);
    }

    async start() {
        this.getId();
        if (!this.id) {
            this.session = {};
            this.create_id();
        } else {
            let session = await this.store.get(this.id);
            if (session === undefined) {
                this.session = {};
            } else {
                this.session = this.decode(session);
                // check session must be a no-null object
                if (typeof this.session !== "object" || this.session == null) {
                    this.session = {};
                }
            }

        }
        this.ctx.session = this.session;
        return Promise.resolve();
    }

    async write() {
        this.setId();
        await this.store.set(this.id, this.encode(this.session), this.options.maxAge / 1e3)
    }

    async destroy() {
        await this.store.destroy(this.id);
    }


}


export default function middleware<E, D>(key: string, store: InterfaceSessionStore<D>, cookie: InterfaceSessionCookie, generate?: () => string) {

    return async function (ctx: Context, next: () => Promise<void>) {

        let session = ctx._session = new SessionContext<E, D>(ctx, key, cookie, store, generate);
        // debug.info("step:1 start", ctx.cookies.get(key), key);
        await session.start();
        // debug.info("step:2 read", ctx.session);
        // debug.info("step:3 before", ctx.session, ctx._session.session);
        await next();
        // debug.info("step:4 after", ctx.session, ctx._session.session);
        await session.write();
        // debug.info("step:5 write", ctx.session);
    }
}