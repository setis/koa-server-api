import * as RedisStorage from "ioredis";
import {Redis, RedisOptions} from "ioredis";
import {InterfaceSessionStore} from "../index";
import  {getInstanceSource} from "../../source";


export default class RedisStore implements InterfaceSessionStore<string> {
    store: Redis;

    constructor(config: RedisOptions | string) {
        let source = getInstanceSource();
        if (typeof config === "string" && source.has(config)) {
            source.use(config).then(value => {
                this.store = value;
            })
        } else {
            this.store = new RedisStorage(config);
        }

    }

    async get(sid: string): Promise<string | undefined> {
        return await this.store.get(`SESSION:${sid}`);
    }

    async set(sid: string, session: string, exp: number): Promise<void> {
        await this.store.set(`SESSION:${sid}`, session, "EX", exp);
    }

    async destroy(sid: string): Promise<void> {
        await this.store.del(`SESSION:${sid}`);
    }
}
