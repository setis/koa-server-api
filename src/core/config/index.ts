import * as configYaml from "config-yaml";
import {AuthenticateOptions} from "passport";
import {ConnectionOptions} from "typeorm";
import {RedisOptions} from "ioredis";
import {Environment} from "../env";
import {ConfigRoutes} from "./routes";
import {ConfigMiddlewares} from "./middlewares";


export interface Config {
    listen: {
        ip: string,
        port: number
    };
    middlewares: ConfigMiddlewares;
    parameters: {
        host: string;
    };
    mode: {
        components: {
            [name: string]: "enable" | "disable" | boolean;
        };
        middlewares: {
            [name: string]: "enable" | "disable" | boolean;
        };
        plugins: {
            [name: string]: "enable" | "disable" | boolean;
        };
    }
    source: {
        [name: string]: {
            use: "ioredis";
            config: RedisOptions;
        } | {
            use: string;
            config: any;
        }
    };
    plugins: {
        passport: {
            uri: string
            host?: string
            strategy: {
                [strategy: string]: {
                    clientID: string,
                    clientSecret: string,
                    options: AuthenticateOptions
                }
            }
        };
    }

    typeorm: {
        [name: string]: ConnectionOptions
    };

    routes: ConfigRoutes


}
export const config = loader();
export function loader(): Config {
    let cfg: Config[] = [];
    [
        `${Environment.DIR.CONFIG}/${Environment.Name()}.yaml`,
        `${Environment.DIR.CONFIG}/default.yaml`
    ].forEach((value, index) => {
        try {
            cfg[index] = configYaml(value);
        } catch (e) {

            cfg[index] = {};
            console.log(`${e.message} path:${value}`);
        }
    });

    return {...cfg[1], ...cfg[0]};
}


export default config;