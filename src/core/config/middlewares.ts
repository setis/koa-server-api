import {IHelmetConfiguration} from "helmet";
import {RedisOptions} from "ioredis";
import {Context} from "koa";
import * as multer from "koa-multer";
import {InterfaceUploadConfig} from "../upload";

export interface ConfigBodyV1 {
    fields: boolean | string;
    files: boolean | string;
    multipart: boolean;
    textLimit: string;
    formLimit: string;
    urlencodedLimit: string;
    jsonLimit: string;
    bufferLimit: string;
    jsonStrict: boolean;
    detectJSON: (ctx: Context) => void;
    strict: boolean;
    onerror: (err, ctx: Context) => void
    extendTypes?: {
        json?: string[];
        form?: string[];
        text?: string[];
    };
    IncomingForm: any;
    handler: (ctx, options, next) => void
    querystring: any;
    qs: any;
    delimiter: string;
    sep: string;
    buffer: string
}

export interface ConfigBodyV0 {
    /**
     *  parser will only parse when request type hits enableTypes, default is ['json', 'form'].
     */
    enableTypes?: string[];
    /**
     * requested encoding. Default is utf-8 by co-body
     */
    encode?: string;

    /**
     * limit of the urlencoded body. If the body ends up being larger than this limit
     * a 413 error code is returned. Default is 56kb
     */
    formLimit?: string;

    /**
     * limit of the json body. Default is 1mb
     */
    jsonLimit?: string;

    /**
     * when set to true, JSON parser will only accept arrays and objects. Default is true
     */
    strict?: boolean;

    /**
     * custom json request detect function. Default is null
     */
    detectJSON?: (ctx: Koa.Context) => boolean;

    /**
     * support extend types
     */
    extendTypes?: {
        json?: string[];
        form?: string[];
        text?: string[];
    };
}

export interface ConfigBodyV2 {
    patchNode: boolean;
    patchKoa: boolean;
    jsonLimit: string | number;
    formLimit: string | number;
    textLimit: string | number;
    encoding: string;
    multipart: boolean;
    urlencoded: boolean;
    text: boolean;
    json: boolean;
    formidable: {
        bytesExpected: number;
        maxFields: number;
        maxFieldsSize: number;
        uploadDir: string;
        keepExtensions: boolean;
        hash: string | false;
        multiples: boolean;
        onFileBegin: () => void
    };
    onError: (error: Error, context: Context) => void
    strict: boolean;
}

export interface ConfigMiddlewares {
    body: {
        use: "parser";
        config?: ConfigBodyV0;
    } | {
        use: "better-body";
        config?: ConfigBodyV1;
    } | {
        use: "body";
        config?: ConfigBodyV2;
    } | {
        use: string;
        config?: any
    };
    cors: {
        origin: ((ctx: Context) => string) | string;
        allowMethods: string[];
        exposeHeaders: string[];
    };
    multer: multer.Options;
    helmet: IHelmetConfiguration;
    ratelimit: {
        /**
         * The identifier to limit against (typically a user id)
         */
        id: "ip" | "ip+ua" | "ip+sid+ua";

        /**
         * Redis connection instance
         */
        db: RedisOptions;

        rule: {
            /**
             * Max requests within duration
             */
            max?: number;

            /**
             * Duration of limit in milliseconds
             */
            duration?: number;
        };

        headers: {
            remaining: string,
            reset: string,
            total: string
        };

        errorMessage: string
    };
    session: {
        key: string,
        store: "redis",
        cookie: {
            maxAge: number,
            expires: number,
            path: string,
            domain?: string,
            secure: boolean,
            httpOnly: boolean,
            signed: boolean,
            overwrite: boolean,
        },
        storage: {
            redis: RedisOptions
        }
    };
    file: InterfaceUploadConfig
}
