import {ConfigMiddlewares} from "./middlewares";

export interface ConfigRoutes {
    [path: string]: {
        [method: string]: {
            operationId: string
            tags?: string[]
            description?: string
            parameters: {
                name: string,
                in: "body" | "query" | "path"
                required?: boolean
                default?: any
                type?: any
                format?: any
                description?: string
            }[];
            middlewares: ConfigMiddlewares | ConfigRouteMiddleware;
            security?: {
                [auth: string]: string[]
            }
            responses?: {
                [status: number]: {
                    description?: string
                    schema?: any
                    items?: any
                    headers?: {
                        [header: string]: {
                            description?: string
                            type?: any
                        }
                    }
                }
            }


        }
    }
}

export interface ConfigRouteMiddleware {
    [name: string]: "enable" | "disable" | boolean;
}
