import {KernelBootstrap} from "./bootstrap";

const kernel = new KernelBootstrap();
kernel.init();
export default kernel;