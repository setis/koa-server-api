import * as Koa from "koa";
import {Context, Middleware} from "koa";
import logger from "./logger";
import * as Router from "koa-router";
import {Config} from "../config";
import {Environment} from "../env";
import {KernelBootstrap, middlewares} from "./bootstrap";
import {promisify} from "util";
import {join} from "path";
import {readdir} from "fs";

export class KernelHttp {
    controllers: Map<string, (ctx: Context, next: () => Promise<void>) => Promise<void>> = new Map();
    config: Config;
    app: Koa;


    constructor(
        public kernel: KernelBootstrap
    ) {
        this.app = this.kernel.app;
        this.config = this.kernel.config;
    }

    static parameters(parameters: any, info: { uri?: string, action?: string, method?: string }): (ctx: Context, next: () => Promise<void>) => Promise<void> {
        return async (ctx: Context, next: () => Promise<void>) => {
            let data = {};
            for (let params of parameters) {
                let value;
                let name = params.name;
                switch (params.in) {
                    case "path":
                        value = ctx.params[name];
                        break;
                    case "query":
                        value = ctx.query[name];
                        break;
                    case "body":
                        try {
                            value = ctx.request.body[name];
                        } catch (e) {
                            logger.error(`parameters: ошибка метода ${params.in}:${name} ctx.request.body[${name}]`);
                        }

                        break;
                    case "file":
                        try {
                            // value = ctx.request.body.files[name];
                            value = (ctx.request.body.files[name] === undefined)
                        } catch (e) {
                            logger.error(`parameters: ошибка метода ${params.in}:${name} ctx.request.body.files[${name}];`);
                        }

                        break;
                    default:
                        logger.error(`parameters: нет такого метода ${params.in}:${name}`);
                    // throw new Error(`нет такого метода ${params.in}:${name}`);

                }
                if (value === undefined) {
                    if (params.hasOwnProperty("default")) {
                        value = params.default;
                    } else {
                        if (params.hasOwnProperty("required") && params.required) {
                            logger.error(`parameters: не хватает данных ${params.in}:${name}`)
                            // throw new Error(`не хватает данных ${params.in}:${name}`);
                        }
                    }

                } else {
                    switch (params.type) {
                        case "number":
                            value = parseInt(value);
                            break;
                        case "string":
                            value = value.trim().toString("utf-8");
                            break;
                        default:
                            logger.error(`parameters: нет такого типа ${params.type}`, params)
                        // throw new Error(`нет такого типа ${params.type}`);

                    }
                }
                data[name] = value;
            }
            logger.info(`${info.method} ${info.uri} -> ${info.action} parameters:`, data);
            ctx.parameters = data;
            await next();


        };
    }

    async loader() {
        await this.controller();
        await this.routes();
        await this.listen();
    }

    listen() {
        let {ip, port} = this.config.listen;
        return new Promise(resolve => {
            this.app.listen(port, ip, () => {
                console.log(`API server listening on ${ip}:${port}`);
                resolve();
            });
        });

    }

    async controller(app: Koa = this.app, dir: string = Environment.DIR.CONTROLLER) {
        const list = await promisify(readdir)(dir);
        await Promise
            .all(
                list
                // .filter(file => {
                //     return (parse(file).base !== "index");
                // })
                    .map(async file => {
                        let path = join(dir, file);
                        let controllers: { [index: string]: (ctx: Context, next: () => Promise<void>) => Promise<void> };
                        try {
                            controllers = await require(path).default(app);
                        } catch (e) {
                            logger.error(`controller not loading path:${path} msg:${e.message}`, e.stack);
                            return;
                        }
                        for (let name in controllers) {
                            this.controllers.set(name, controllers[name]);
                        }
                        logger.log(`controller loading path:${file} actions:${Object.keys(controllers).join(",")}`);

                    })
            );
        logger.info(`controllers:${this.controllers.size}  list:${Array.from(this.controllers.keys()).join(",")}`)

    }

    async route(router: Router, cfg: any, method: string, uri: string) {
        if (router[method] === undefined) {
            logger.error(`нет такого метода:${method}  ${cfg.operationId} -> ${method.toUpperCase()}:${uri}`);
            return;
        }
        let controller = this.controllers.get(cfg.operationId);
        if (!this.controllers.has(cfg.operationId) || controller === undefined) {
            logger.error(`нет такого события  ${cfg.operationId} -> ${method.toUpperCase()}:${uri}`);
            return;
            // throw new Error(`нет такого события ${cfg.operationId}`);
        }

        let rules: ((ctx: Context, next: () => Promise<void>) => Promise<void>)[] = [];
        let info = {uri: uri, action: cfg.operationId, method: method};
        let middlewares = {after: [], before: []};
        if (cfg.middlewares && Object.keys(cfg.middlewares).length > 0) {
            middlewares = await this.middlewares(cfg.middlewares, info);
            logger.log(`controller: ${cfg.operationId} -> ${method.toUpperCase()}:${uri} found middlewares`);
        } else {
            logger.log(`controller: ${cfg.operationId} -> ${method.toUpperCase()}:${uri} not found middlewares`);
        }
        rules.push(async (ctx: Context, next: () => Promise<void>) => {
            logger.info(`controller: ${cfg.operationId} -> ${method.toUpperCase()}:${uri}`);
            await next();
        });
        logger.log(`controller: ${cfg.operationId}  parameters: ${(cfg.parameters) ? "y" : "n"}`);
        if (cfg.parameters) {
            logger.log(`controller: ${cfg.operationId} parameters: `, cfg.parameters);
            rules.push(KernelHttp.parameters(cfg.parameters, info));
            rules.push(async (ctx: Context, next: () => Promise<void>) => {
                logger.info(`controller: ${cfg.operationId}(${JSON.stringify(ctx.parameters)})`);
                await next();
            });
        } else {
            rules.push(async (ctx: Context, next: () => Promise<void>) => {
                logger.info(`controller: ${cfg.operationId}()`);
                ctx.set(`X-Controller`, cfg.operationId);
                await next();
            });
        }

        rules.push(controller);
        // if (cfg.security !== undefined && cfg.security instanceof Array && cfg.security.length > 0) {
        //     rules.unshift(AclRoute(cfg.security[0]));
        // }

        router[method](uri, ...middlewares.before, ...rules, ...middlewares.after);
        logger.info(`route: ${cfg.operationId} -> ${method.toUpperCase()}:${uri}`);
    }

    async routes(app: Koa | Router = this.app, config: any = this.config.routes) {
        let router = new Router();
        let routes = [];
        for (let uri in config) {
            for (let method in config[uri]) {
                /**
                 * заменяем uri swagger -> express
                 * @example /new/{id} -> /new/:id
                 * @type {string}
                 */
                let uriNew = uri.replace(/(\{(.*?)\})/g, (str, p1, p2, offset, s) => {
                    return `:${p2}`;
                });
                let cfg = config[uri][method];
                routes.push(this.route(router, cfg, method, uriNew));
            }
        }
        await Promise.all(routes);
        console.log(app);
        app
            .use(router.routes())
            .use(router.allowedMethods());
    }

    async middlewares(middlewares_config: any, info: { uri?: string, action?: string, method?: string }): Promise<{
        after: Middleware[],
        before: Middleware[]
    }> {
        let list = {after: [], before: []};
        for (let name in middlewares_config) {
            let config = middlewares_config[name];
            switch (config) {
                case "enable":
                case true:
                case "true":
                    config = this.config.middlewares[name];
                    logger.info(`${info.method} ${info.uri} -> middleware: ${name} enable`, config);
                    break;
                case "disable":
                case false:
                case "false":
                    logger.info(`${info.method} ${info.uri} -> middleware: ${name} disable`);
                    continue;
            }
            try {
                let middleware = require(`${Environment.DIR.MIDDLEWARE}/${name}`);
                let mList = await middleware.list(config);
                if (middlewares.after.indexOf(name) !== -1) {

                    list.after = list.after.concat([async (ctx, next) => {
                        logger.info(`${info.method} ${info.uri} -> middleware: ${name}`);
                        await next();
                    }, ...mList]);
                }
                else if (middlewares.before.indexOf(name) !== -1) {
                    list.before = list.before.concat([async (ctx, next) => {
                        logger.info(`${info.method} ${info.uri} -> middleware: ${name}`);
                        await next();
                    }, ...mList]);
                }
                else {
                    throw new Error(`нету приоритета для ${name}`);
                }
                logger.info(`${info.method} ${info.uri} -> middleware: ${name}  config:`, config);
            } catch (e) {
                logger.error(`${info.method} ${info.uri} -> middleware: ${name} error:${e.message}`, e);
                // logger.error(e.message, e);
                continue;
            }
        }
        return list;
    }


}