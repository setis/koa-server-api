import * as Debug from "debug";
const logger = {
    log: Debug("kernel:http:log"),
    info: Debug("kernel:http:info"),
    error: Debug("kernel:http:error"),
};
export default logger;