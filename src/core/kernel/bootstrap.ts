import {Environment} from "../env";
import logger from "./logger";
import {Config} from "../config";
import * as Koa from "koa";
import {KernelHttp} from "./http";
import app from "./app";

export const type = {
    components: [
        "orm", "redux", "graphql", "file"
    ],
    middlewares: [
        "helmet", "cors", "body", "session", "useragent", "x-response-time", "error", "ratelimit", "favicon"
    ],
    plugins: [
        "passport"
    ]
};
export const priorities = [
    [
        "orm"
    ],
    [
        "cors", "session", "body", "useragent", "x-response-time", "favicon", "body"
    ],

    [
        "passport", "error", "ratelimit"
    ],

];
export const middlewares = {
    before: [
        "multer", "helmet", "session", "useragent", "ratelimit", "body", "cors"
    ],
    after: [
        "x-response-time", "error", "favicon"
    ]
};

export function status(state: Object, name: string): boolean {
    let result = false;
    switch (state[name]) {
        case "enable":
        case true:
            result = true;
            break;
        case "disable":
        case false:
            result = false;
            break;
    }
    return result;
}

export class KernelBootstrap {

    config: Config;
    http: KernelHttp;
    app: Koa;
    constructor(
    ) {
        this.component = this.component.bind(this);
        this.middleware = this.middleware.bind(this);
        this.plugin = this.plugin.bind(this);
    }

    async init() {
        this.app = app;
        this.config = require("../config").config;
        this.http = new KernelHttp(this);
        await this.loader();
        await this.http.loader();
    }

    async loader(priority: boolean = true) {
        for (let f of (priority) ? this.ListPriority() : this.ListNoPriority()) {
            await f();
        }
    }

    async middleware(value: string, parameters: { app: Koa | Router, config?: any }): Promise<void> {
        let path = `${Environment.DIR.MIDDLEWARE}/${value}`;
        let {app, config} = {app: this.app, ...parameters};
        try {
            await require(path).set(this.app, config);
            logger.log(`loaded middleware name:${value}`);
        } catch (e) {
            logger.error(`not load middleware name:${value} msg:${e.message} path:${path}`);
        }
    }

    async plugin(value: string, parameters: { app: Koa | Router, config?: any, params?: any }): Promise<void> {
        let path = `${Environment.DIR.PLUGIN}/${value}`;
        let {app, config, params} = {app: this.app, ...parameters};
        try {
            await require(path).default(app, config, params);
            logger.log(`loaded plugin name:${value}`);
        } catch (e) {
            logger.error(`not load plugin name:${value} msg:${e.message} path:${path}`);
        }
    }

    async component(value: string, parameters: { app: Koa | Router, config?: any }) {
        let path = `${Environment.DIR.KERNEL}/${value}`;
        let {app, config} = {app: this.app, ...parameters};
        try {
            await require(path).default(app, config);
            logger.log(`loaded component name:${value}`);
        } catch (e) {
            logger.error(`not load component name:${value} msg:${e.message}`);
        }
    }

    protected ListNoPriority() {
        let loading = [];
        let mode = this.config.mode;
        loading.push(async () => {
            await Promise
                .all(type.components
                    .filter(value => {
                        return status(mode.components, value);
                    })
                    .map(value => {
                        return this.component(value, {
                            app: this.app,
                            config: this.config[value]
                        });
                    }))
        });
        loading.push(async () => {
            await Promise
                .all(type.middlewares
                    .filter(value => {
                        return status(mode.middlewares, value);
                    })
                    .map(value => {
                        return this.middleware(value, {
                            app: this.app,
                            config: this.config.middlewares[value]
                        });
                    }));
        });
        loading.push(async () => {
            await Promise
                .all(type.plugins
                    .filter(value => {
                        return status(mode.plugins, value);
                    })
                    .map(value => {
                        return this.plugin(value, {
                            app: this.app,
                            config: this.config.plugins[value],
                            params: this.config.parameters
                        });
                    }));
        });
        return loading;
    }

    protected ListPriority() {
        let loading = [];
        let mode = this.config.mode;
        loading = priorities
            .filter(value => {
                return value.length > 0;
            })
            .map(value => {
                return async () => {
                    return await Promise.all(value.map(value2 => {
                        if (type.components.indexOf(value2) !== -1) {
                            return this.component(value2, {
                                app: this.app,
                                config: this.config[value2]
                            });
                        }
                        else if (type.middlewares.indexOf(value2) !== -1) {
                            if (!status(mode.middlewares, value2)) {
                                logger.info(`middleware disable:${value2}`);
                            } else {
                                return this.middleware(value2, {
                                    app: this.app,
                                    config: this.config.middlewares[value2]
                                });
                            }

                        }
                        else if (type.plugins.indexOf(value2) !== -1) {
                            if (!status(mode.plugins, value2)) {
                                logger.info(`plugin disable:${value2}`);
                            } else {
                                return this.plugin(value2, {
                                    app: this.app,
                                    config: this.config.plugins[value2],
                                    params: this.config.parameters
                                });
                            }

                        } else {
                            logger.error(`bootstrap not load ${value2}`);
                            return Promise.resolve();
                        }
                    }));
                };
            });
        return loading;
    }


}
