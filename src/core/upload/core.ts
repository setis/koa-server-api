import {UploadStorageFile} from "./storage";
import {InterfaceUploadConfig, InterfaceUploader} from "./index";
import * as hash from "hash-stream";

const config = {
    etag: {
        algorithm: "sha256",
        encoding: "base64"
    },
    default: "fs",
    storage: {
        fs: {
            store: "file",
            config: {
                dir: __dirname,
                via: "x-accel-redirect",
                cache: {
                    mode: true,
                    store: "file",
                    config: {
                        dir: __dirname,
                        ttl: 60 * 60 * 24,
                        timeout: 5e3,
                    }
                }
            }
        }
    }
};

export class UploadCore {
    config: InterfaceUploadConfig;
    storages: Map<string, InterfaceUploader>;

    constructor(cfg: InterfaceUploadConfig = config) {
        this.config = cfg;
        this.storages = new Map();
        this.loader();

    }

    loader() {
        Object.keys(this.config.storage)
            .forEach(name => {
                let {store, config} = this.config.storage[name];
                this.load(name, store, config);
            })
    }

    load(name: string, store: string, config: any) {
        let storage;
        switch (store) {
            case "file": {
                storage = new UploadStorageFile(config, this.config.etag);
            }
            default: {
                throw new Error(`не найдено такого storage:${name}`);
            }
        }
        this.storages.set(name, storage);
        return storage;
    }

    async etag(path: any): Promise<string> {
        let {algorithm, encoding} = this.config.etag;
        let data = await hash(path, algorithm);
        return data.toString(encoding);
    }

    async storage(name: string = this.config.default) {
        let storage;
        if (!this.storages.has(name)) {
            let {store, config} = this.config.storage[name];
            storage = this.load(name, store, config);
        } else {
            storage = this.storages.get(name);
        }
        if (!storage.loaded) {
            await this.loader();
        }
        return storage;
    }

}