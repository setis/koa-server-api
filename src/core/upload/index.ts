import {Context} from "koa";
import {RedisOptions} from "ioredis";
import {UploadCore} from "./core";

export interface InterfaceUploadConfig {
    etag: {
        algorithm: string;
        encoding: string;
    };
    default:string;
    storage: {
        [name: string]: {
            store: string | "file";
            config: {
                dir: string,
                via: string | "x-accel-redirect" | "native",
                cache: {
                    mode: boolean,
                    store: "file",
                    config: {
                        dir: string;
                        ttl?: number;
                        expeire?: any;
                        timeout: number;
                    };
                } | {
                    mode: boolean,
                    store: "redis",
                    config: RedisOptions;
                } | {
                    mode: boolean,
                    store: string,
                    config: any;
                }
            }
        }
    };


}

export interface InterfaceUploader {
    loaded: boolean;

    loader(): Promise<void>;

    down(ctx: Context, data: string): Promise<void>;

    up(ctx: Context): Promise<void>
}
export interface InterfaceUploadStorageFileGenerate {
    identity: {
        headers: { [header: string]: string };
        path: string
    };
    gzip: {
        headers: { [header: string]: string };
        path: string;
    } | false;

}

let instance: UploadCore;

export default function getInstanceUpload() {
    if (instance === undefined) {
        instance = new UploadCore();
    }
    return instance;
}

export * from "./cache";
export * from "./storage";