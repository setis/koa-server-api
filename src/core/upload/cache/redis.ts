import {InterfaceUploadCache} from "./index";
import {InterfaceUploadStorageFileGenerate} from "../storage/file";
import * as RedisStorage from "ioredis";
import {Redis, RedisOptions} from "ioredis";
import {getInstanceSource} from "../../source";

export class UploadCacheRedis implements InterfaceUploadCache<InterfaceUploadStorageFileGenerate> {
    store: Redis;

    constructor(config: RedisOptions | string) {
        let source = getInstanceSource();
        if (typeof config === "string" && source.has(config)) {
            source.use(config).then(value => {
                this.store = value;
            })
        } else {
            this.store = new RedisStorage(config);
        }

    }

    async loader() {
        await this.store.connect();
    }

    async has(id: string): Promise<boolean> {
        const data = await this.store.keys(id);
        return (data.length > 0);
    }

    async get(id: string) {
        let data = await this.store.get(id);
        return JSON.parse(data);
    }

    async set(id: string, data, expires?: number): Promise<void> {
        let args = (expires) ? ["EX", expires.toString()] : [];
        await this.store.set(id, JSON.stringify(data), ...args);
    }

    async del(id: string): Promise<void> {
        await this.store.del(id);
    }
}