export interface InterfaceUploadCache<T> {
    loader(): Promise<void>;

    set(id: string, data: T, expires?: number): Promise<void>;

    has(id: string): Promise<boolean>;

    get(id: string): Promise<T>;

    del(id: string): Promise<void>;
}

export {UploadCacheFile, InterfaceUploadFileCache} from "./file";
export {UploadCacheRedis} from "./uploadCacheRedis";
