import {fs} from "mz";
import {InterfaceUploadCache} from "./index";
import {InterfaceUploadStorageFileGenerate} from "../storage/file";

export interface InterfaceUploadFileCache {
    data: InterfaceUploadStorageFileGenerate;
    expires?: number;
}

export class UploadCacheFile implements InterfaceUploadCache<InterfaceUploadStorageFileGenerate> {
    pool: Map<string, InterfaceUploadFileCache> = new Map<string, InterfaceUploadFileCache>();
    timer: Map<string, number> = new Map<string, number>();

    id: any;

    constructor(public config: {
        dir: string;
        ttl?: number;
        expires?: number;
        timeout: number;
    }) {
    }

    loader(): Promise<void> {
        this.id = setInterval(() => {
            let time = new Date().getTime();
            this.timer
                .forEach((expire, path) => {
                    if (time > expire) {
                        this.pool.delete(path);
                        this.timer.delete(path);
                    }
                })
        }, this.config.timeout);
        this.id.unref();
        return Promise.resolve();
    }

    path(pathOriginal: string): string {
        return pathOriginal + ".cache";
    }


    async set(pathOriginal: string, data, expires?: number) {
        let path = this.path(pathOriginal);
        await fs.writeFile(path, JSON.stringify({
            data: data,
            expires: expires
        }));


    }

    async has(pathOriginal: string): Promise<boolean> {
        let path = this.path(pathOriginal);
        let stats;
        try {
            stats = await fs.stat(path);
        } catch (e) {
            return false;
        }
        if (!stats) {
            return false;
        }
        if (!stats.isFile()) {
            throw new Error(`не является файлом ${path}`);
        }
        let file = await fs.readFile(path);
        let data: InterfaceUploadFileCache = JSON.parse(file.toString());
        if (data.expires && data.expires < new Date().getTime()) {
            try {
                await fs.unlink(path);
            } catch (e) {

            }
            return false;
        }
        this.pool.set(path, data);
        this.timer.set(path, new Date().getTime());
        return true;

    }

    async get(pathOriginal: string) {
        let path = this.path(pathOriginal);
        if (this.pool.has(path)) {
            setTimeout(() => {
                this.pool.delete(path);
                this.timer.delete(path);
            }, 25);
            return this.pool.get(path).data;
        }
        let stats;
        try {
            stats = await fs.stat(path);
        } catch (e) {

        }
        if (!stats) {
            return false;
        }
        if (!stats.isFile()) {
            throw new Error(`не является файлом ${path}`);
        }
        let file = await fs.readFile(path);
        return JSON.parse(file.toString()).data;
    }

    del(pathOriginal: string): Promise<void> {
        let path = this.path(pathOriginal);
        this.pool.delete(path);
        this.timer.delete(path);
        return Promise.resolve();
    }

}
