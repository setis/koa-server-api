import * as compressible from "compressible";
import {createGzip} from "zlib";
import {createReadStream, createWriteStream, Stats} from "fs";
import {fs} from "mz";
import * as mime from "mime-types";
import {Context} from "koa";
import {basename, extname, relative} from "path";
import * as hash from "hash-stream";
import {InterfaceUploadCache, UploadCacheFile, UploadCacheRedis} from "../cache";
import {RedisOptions} from "ioredis";
import {InterfaceUploader, InterfaceUploadStorageFileGenerate} from "../index";


export class UploadStorageFile implements InterfaceUploader {

    loaded: boolean;
    cache: InterfaceUploadCache<InterfaceUploadStorageFileGenerate>;

    constructor(public config: {
                    dir: string,
                    via: "x-accel-redirect" | "native",
                    cache: {
                        mode: boolean,
                        store: "file",
                        config: {
                            dir: string;
                            ttl?: number;
                            expeire?: any;
                            timeout: number;
                        };
                    } | {
                        mode: boolean,
                        store: "redis",
                        config: RedisOptions;
                    } | {
                        mode: boolean,
                        store: string,
                        config: any;
                    }
                },
                public etag: {
                    algorithm: string;
                    encoding: string;
                }
    ) {
    }

    async hash(path: string): Promise<string> {
        let {algorithm, encoding} = this.etag;
        let data = await hash(path, algorithm);
        return data.toString(encoding);
    }

    async loader() {
        let {mode, store, config} = this.config.cache;
        if (mode) {
            switch (store) {
                case "file": {
                    this.cache = new UploadCacheFile(config);
                }
                case "redis": {
                    this.cache = new UploadCacheRedis(config);
                }
            }
            await this.cache.loader();
        }
        this.loaded = true;
    }

    temp(path: string) {
        return path + "." + Math.random().toString(36).slice(2) + ".gz";
    }

    async compress(pathOriginal: string, size: number): Promise<{
        stats: Stats,
        path: string;
    }> {
        // if we can compress this file, we create a .gz
        let path = pathOriginal + ".gz";
        let stats;
        try {
            stats = await fs.stat(path);
        } catch (e) {

        }
        if (stats) {
            if (!stats.isFile()) {
                throw new Error(`не является файлом ${path}`);
            }
            else if (size > stats.size) {
                // delete old .gz files in case the file has been updated
                await fs.unlink(path)
            }
            else {
                return {
                    path: path,
                    stats: stats
                }
            }

        }
        // save to a random file name first
        let tmp = this.temp(pathOriginal);
        await new Promise((resolve, reject) => {
            createReadStream(path)
                .on("error", reject)
                .pipe(createGzip())
                .on("error", reject)
                .pipe(createWriteStream(tmp))
                .on("error", async (...args) => {
                    await fs.unlink(tmp);
                    reject(...args);
                })
                .on("finish", resolve)
        });
        await fs.rename(tmp, path);
        stats = await fs.stat(path);

        return {
            path: path,
            stats: stats
        }

    }

    async file(path: string): Promise<{
        stats: Stats;
        etag: string;
        type: string;
        compress?: {
            path: string;
            stats: Stats;
        };
    }> {
        const stats = await fs.stat(path);
        if (!stats || !stats.isFile()) return;
        let hash = await this.hash(path);
        let data = {
            stats: stats,
            etag: hash,
            type: mime.contentType(extname(path)) || "application/octet-stream",
        };

        if (!compressible(data.type)) return data;
        try {
            let compress = await this.compress(path, stats.size);
            data.compress = compress;
        } catch (e) {

        }
        return data
    }

    up(ctx: Context): Promise<void> {
        return Promise.resolve();
    }

    async generate(path: string): Promise<InterfaceUploadStorageFileGenerate> {
        let file = await this.file(path);
        let headers = {
            "Content-Type": file.type,
            etag: file.etag,
            "Last-Modified": file.stats.mtime.toUTCString(),
        };
        return {
            identity: {
                headers: {
                    ...headers,
                    "Content-Encoding": "identity",
                    "Content-Length": file.stats.size.toString()
                },
                path: path,


            },
            gzip: (file.compress) ? {
                headers: {
                    ...headers,
                    "Content-Encoding": "gzip",
                    "Content-Length": file.compress.stats.size.toString()
                },
                path: file.compress.path
            } : false,
        };
    }

    expires(): number {
        let {ttl, expires} = this.config.cache.config;
        if (ttl) {
            return (new Date().getTime()) + ttl * 1e3
        }
        else if (expires) {
            /**
             * @todo время когда кэш менять
             */
            throw new Error()
        }

    }

    async down(ctx: Context, path: string): Promise<void> {
        let {mode} = this.config.cache;
        let status = (mode) ? await this.cache.has(path) : false;

        let data = await ((mode && status) ? this.cache.get(path) : this.generate(path));
        let headers, file;
        if (mode && !status) {
            this.cache.set(path, data, this.expires());
        }
        if (data.gzip && ctx.acceptsEncodings("gzip", "identity") === "gzip") {
            headers = data.gzip.headers;
            file = data.gzip.path;
        } else {
            headers = data.identity.headers;
            file = data.identity.path;
        }

        switch (this.config.via) {
            case "x-accel-redirect": {
                headers["Content-Disposition"] = `attachment; filename="${basename(file)}"`;
                headers["X-Accel-Redirect"] = relative(this.config.dir, file);
                for (let k in headers) {
                    ctx.res.setHeader(k, headers[k]);
                }
                ctx.res.end("");
            }
            default:
            case "native":
                let fresh = ctx.request.fresh;
                switch (ctx.request.method) {
                    case "HEAD":
                        ctx.response.status = fresh ? 304 : 200;
                        break;
                    case "GET":
                        if (fresh) {
                            ctx.response.status = 304
                        } else {
                            ctx.body = createReadStream(file)
                        }
                        break
                }
                break;
        }


    }
}