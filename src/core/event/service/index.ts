import {EventServiceKernel} from "./kernel";

export * from "./kernel";
export * from "./cache";

let instance: EventServiceKernel<any, any>;

export function getInstance(): EventServiceKernel<any, any> {
    if (!instance) {
        instance = new EventServiceKernel();
    }
    return instance;
}