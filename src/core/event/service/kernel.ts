export class EventServiceKernel<P, R> {
    listeners: Map<string, (params: any) => Promise<any>>;


    constructor() {
        this.listeners = new Map();

    }

    has(name: string): boolean {
        return this.listeners.has(name);
    }

    set(name: string, listener): this {
        this.listeners.set(name, listener);
        return this;
    }

    del(name: string): this {
        this.listeners.delete(name);
        return this;
    }

    get(name: string): (parameters?: any) => any {
        let container = this.listeners.get(name);
        if (container === undefined) {
            throw new Error(`нет данного события ${name}`);
        }
        return container;
    }

    async runner<R, P>(name: string, parameters?: P): Promise<{
        status: "success" | "error",
        event: {
            name: string,
            parameters?: P
        },
        data?: R,
        error?: {
            msg: string,
            name: string,
            stack?: string
        }
    }> {
        let result: any;
        try {
            result = await this.get(name)(parameters);
        } catch (e) {
            return {
                status: "error",
                event: {
                    name: name,
                    parameters: parameters
                },
                error: {
                    msg: e.message,
                    stack: e.stack,
                    name: e.name
                }

            };
        }
        return {
            status: "success",
            event: {
                name: name,
                parameters: parameters
            },
            data: result

        };

    }
}