interface CacheStore {

    setID(id: string): string;

    packData(data: any): Promise<any>;

    unpakDate(data: any): Promise<any>;

    set(name: string, data: any, ttl?: number): Promise<void>;

    has(name: string): Promise<boolean>;

    del(name: string): Promise<void>;

    get(name: string): Promise<any>;

    clear(): Promise<any>
}

export class EventServiceCache {
    listeners: Map<string, Set<string>>;

    on(name: string, events: string | string[]): this {
        let container;
        let f = true;
        if (this.listeners.has(name)) {
            container = this.listeners.get(name);
            f = false;
        }
        if (container === undefined) {
            container = new Set();
        }
        if (events instanceof Array) {
            events.forEach(value => {
                container.add(value);
            });
        } else {
            container.add(events);
        }

        if (f) {
            this.listeners.set(name, container);
        }
        return this;
    }

    off(name: string, events?: string | string[]): this {
        let container;
        if (this.listeners.has(name)) {
            container = this.listeners.get(name);
        }
        if (container === undefined) {
            return this;
        }
        if (events instanceof Array) {
            events.forEach(value => {
                container.delete(value);
            });
        } else {
            container.delete(events);
        }
        return this;
    }

    async set(name: string, parameters: any) {

    }

}
