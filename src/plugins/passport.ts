import * as Koa from "koa";
import * as passport from "koa-passport";
import * as Router from "koa-router";
import {Strategy as GithubStrategy} from "passport-github";
import {Strategy as BitbucketStrategy} from "passport-bitbucket";
import {Strategy as FaceBookStrategy} from "passport-facebook";
import {Strategy as GoogleStrategy} from "passport-google";
import {Strategy as TwitterStrategy} from "passport-twitter";
import {Strategy as VkontakteStrategy} from "passport-vkontakte";
import {Strategy as YandexStrategy} from "passport-yandex";
import {Strategy as SlackStrategy} from "passport-slack";
import {Strategy as TelegramStrategy} from "passport-telegram";
import {Strategy as MailStrategy} from "passport-mail";
import {Strategy as InstagramStrategy} from "passport-instagram";
import {Strategy as LinkedinStrategy} from "passport-linkedin";
import {Strategy as GitlabStrategy} from "passport-gitlab2";
import {Strategy as YoutubeStrategy} from "passport-youtube-v3";
import {Strategy as LocalStrategy} from "passport-local";
import {AuthenticateOptions} from "passport";
import {getRepository} from "typeorm";
import index from "../core/config/index";
import {password as Hpassword, response} from "../common/index";
import {PassportStrategy} from "../orm/entities/passport-strategy";
import {PassportLocal} from "../orm/entities/passport-local";
import {User} from "../orm/entities/user";
import * as Debug from "debug";

const debug = {
    log: Debug("middleware:passport:log"),
    info: Debug("middleware:passport:info"),
    error: Debug("middleware:passport:error"),
    debug: Debug("middleware:passport:debug"),
};
const ListStrategy = {
    github: GithubStrategy,
    bitbucket: BitbucketStrategy,
    gitlab: GitlabStrategy,

    google: GoogleStrategy,
    yandex: YandexStrategy,

    youtube: YoutubeStrategy,
    instagram: InstagramStrategy,
    linkedin: LinkedinStrategy,
    mail: MailStrategy,
    facebook: FaceBookStrategy,
    twitter: TwitterStrategy,
    vkontakte: VkontakteStrategy,

    slack: SlackStrategy,
    telegram: TelegramStrategy,


};

export class AuthError extends Error {

}

export default async function app(app: Koa,config:any,parameters?:any) {
    const router = loader(config.uri,
        (config.host === undefined) ? parameters.host : config.host,
        config.strategy);
    app
        .use(passport.initialize())
        .use(passport.session())
        .use(router.routes())
        .use(router.allowedMethods());
    await Promise.resolve();

}

export function loader(basePath: string, host: string, config: {
    [strategy: string]: {
        clientID: string,
        clientSecret: string,
        options: AuthenticateOptions
    }
}): Router {
    const PassportStrategyRepository = getRepository(PassportStrategy);
    const PassportLocalRepository = getRepository(PassportLocal);
    const UserRepository = getRepository(User);
    // passport.serializeUser(function (user, done) {
    //     done(null, user);
    // });
    //
    // passport.deserializeUser(function (user, done) {
    //     done(null, user);
    // });
    passport.serializeUser(function (user: User, done) {
        debug.debug(`passport.serialize`, user);
        done(null, user.id);
    });

    passport.deserializeUser(async function (id: number, done) {
        debug.debug(`passport.deserialize`, id);
        try {
            const user = await UserRepository.findOneById(id);
            debug.debug(`passport.deserialize`, user);
            done(null, user);
        } catch (e) {
            debug.error(`passport.deserialize`, id, e);
            return done(e);
        }
    });
    const router = new Router();
    router
        .use(passport.initialize())
        .use(passport.session());
    let loading: string[] = [];
    for (let name in ListStrategy) {
        let strategy = ListStrategy[name];
        let cfg = config[name];
        if (cfg === undefined) {
            continue;
        }
        if (name === "local") {
            passport.use(new LocalStrategy(
                async function (username: string, password: string, done: Function) {
                    const local = await PassportLocalRepository.findOne({
                        where: {username: username, password: Hpassword.hash(password)},
                        relations: ["user"]
                    });
                    if (!local) {
                        debug.error(`не существуюет пользователя или не верно введен пароль ${username}`);
                        return done(new AuthError(`не существуюет пользователя или не верно введен пароль ${username}`));
                    }
                    // const local = await PassportLocalRepository.findOne({
                    //     where: {username: username},
                    //     relations: ['user']
                    // });
                    // if (!local) {
                    //     return done(new AuthError(`не существуюет пользователя ${username}`));
                    // }
                    // if (!await local.validatePassword(password)) {
                    //     return done(new AuthError(`не ввернно введен пароль ${username}`));
                    // }
                    debug.log(local.user);
                    done(null, local.user);
                }
            ));
            router.post(`${basePath}/${name}`, passport.authenticate("local", cfg.options));
            loading.push(name);
            continue;
        }
        if (cfg.clientID === undefined || cfg.clientSecret === undefined) {
            continue;
        }
        passport.use(new strategy({
                clientID: cfg.clientID,
                clientSecret: cfg.clientSecret,
                callbackURL: `http://${host}${basePath}/${name}/callback`
            },
            async function (accessToken, refreshToken, profile, done) {
                if (!profile || !profile.id) {
                    debug.error(`not found profile.id strategy:${name}`);
                    return done(new AuthError(`not found profile.id strategy:${name}`));
                }
                let data;
                try {
                    data = await PassportStrategyRepository.findOne({
                        where: {
                            strategy: name,
                            profile_id: profile.id
                        },
                        relations: ["user"]
                    });

                } catch (e) {
                    debug.error(`strategy:${name} error:${e.message}`);
                    return done(new AuthError(`strategy:${name} error:${e.message}`));
                }
                if (data === undefined) {
                    let user = new User();
                    try {
                        user.createAt = new Date();
                        user = await UserRepository.save(user);
                        data = new PassportStrategy();
                        data.strategy = name;
                        data.profile_id = profile.id;
                        data.profile = profile;
                        data.accessToken = accessToken;
                        if (refreshToken !== undefined) {
                            data.refreshToken = refreshToken;
                        }
                        data.user = user;
                        data = await PassportStrategyRepository.save(data);

                    } catch (e) {
                        debug.error(e);
                        return done(e);
                    }
                    debug.debug(user);
                    // return done(new AuthError(`not found profile strategy:${name}`), null);
                    return done(null, user);
                }
                debug.debug(data.user);
                return done(null, data.user);
            }));

        router
            .get(`${basePath}/${name}`, passport.authenticate(name, cfg.options))
            .get(`${basePath}/${name}/callback`, passport.authenticate(name, cfg.options), (ctx, next) => {
                debug.info(name)
            });
        debug.info(`strategy: ${name}
            auth: ${basePath}/${name}
            callback: ${basePath}/${name}/callback${(cfg.options.scope) ? `\n\t\tscope: ${cfg.options.scope.join(",")}` : ``}
            success: ${cfg.options.successRedirect}
            fail: ${cfg.options.failureRedirect}`);
        loading.push(name);
    }
    debug.log(`strategies: ${loading.join(", ")}`);
    return router;
}
