import {Environment} from "../core/env";
import middlewareSession from "../core/session";
import * as Debug from "debug";
import compose = require("koa-compose");

const debug = {
    log: Debug("middleware:session:log"),
    info: Debug("middleware:session:info"),
    error: Debug("middleware:session:error"),
    debug: Debug("middleware:session:debug"),
};

export async function list(config) {
    let name = config.store;
    let options = config.storage[name];
    let store;
    try {
        store = require(`${Environment.DIR.SESSION}/${name}`).default;
    } catch (e) {
        debug.error(`middleware session store:${`${Environment.DIR.SESSION}/${name}`} error msg:${e.message}`, e.stack);
        return Promise.reject(`middleware session store:${`${Environment.DIR.SESSION}/${name}`} error msg:${e.message}`);
    }
    if (!Environment.isDev()) {
        return Promise.resolve(middlewareSession<any, string>(config.key, new store(options), config.cookie));
    }
    return Promise.resolve(compose([
        middlewareSession<any, string>(config.key, new store(options), config.cookie),
        async (ctx, next) => {
            // debug.debug(ctx.session,ctx.session.count++);
            (ctx.session.count !== undefined) ? ctx.session.count++ : ctx.session.count = 0;
            debug.debug(ctx.session);
            await next();

        }]));

}

export async function set(app, config) {
    let middleware = await list(config);
    app.use(middleware);
}
