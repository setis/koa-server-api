import {HttpError} from "../core/error";
import * as Koa from "koa";
import {Context} from "koa";

export default async function (app: Koa) {
    app.on("error", (err: HttpError | Error, ctx: Context) => {
        if (err instanceof HttpError) {
            ctx.status = err.code;
            ctx.body = {
                ...err.toObject(),
                user: (ctx.session && ctx.session.user) ? ctx.session.user : null
            };
        } else {
            ctx.status = 500;
            ctx.body = {
                status: "error",
                error: {
                    msg: err.message,
                    name: err.name,
                    stack: err.stack
                },
                user: (ctx.session && ctx.session.user) ? ctx.session.user : null
            };
        }
    //     // ctx.res.end();
    });
    await Promise.resolve();
}