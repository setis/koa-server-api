import * as helmet from "koa-helmet";
import * as Koa from "koa";
import {Middleware} from "koa";
import * as Router from "koa-router";

export function set(app: Koa | Router, config): Promise<void> {
    app.use(helmet(config));
    return Promise.resolve();
}

export function list(config): Promise<Middleware> {
    return Promise.resolve(helmet(config));
}