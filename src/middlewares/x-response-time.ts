import * as Koa from "koa";
import {Middleware} from "koa";
import * as Debug from "debug";
import * as Router from "koa-router";

const debug = {
    log: Debug("middleware:x-response-time:log"),
    info: Debug("middleware:x-response-time:info"),
    error: Debug("middleware:x-response-time:error"),
    debug: Debug("middleware:x-response-time:debug"),
};
async function x_reponse_time(ctx, next) {
        let start = Date.now();
        await next();
        let ms = Date.now() - start;
        debug.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
        ctx.set(`X-Response-Time`, `${ms}ms`);
}

export function set(app: Koa | Router): Promise<void> {
    app.use(x_reponse_time);
    return Promise.resolve();
}

export function list(): Promise<Middleware> {
    return Promise
        .resolve(
                x_reponse_time
        );
    }
