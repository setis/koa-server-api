import * as Koa from "koa";
import {Middleware} from "koa";
import * as Router from "koa-router";
import {join} from "path";
import {Environment} from "../core/env";
import * as Debug from "debug";

const debug = {
    log: Debug("middleware:body:log"),
    info: Debug("middleware:body:info"),
    error: Debug("middleware:body:error"),
    debug: Debug("middleware:body:debug"),
};

function configure(cfg: {
    use: string;
    config?: any
}) {
    let {use, config} = cfg;
    if (config.formidable) {
        config.formidable.uploadDir = join(Environment.DIR.RESOURCE, config.formidable.uploadDir)
    }
    let middleware;
    switch (use) {
        case "parser":
            middleware = require("koa-bodyparser");
            break;
        default:
        case "body":
            middleware = require("koa-body");
            break;
        case "better-body":
            middleware = require("koa-better-body");
            break;
    }
    debug.info(use, config);
    return (config) ? middleware(config) : middleware();
}

export function set(app: Koa | Router, config): Promise<void> {
    app.use(configure(config));
    return Promise.resolve();
}

export function list(config): Promise<Middleware> {
    return Promise.resolve(configure(config));
}