import ratelimit = require("koa-ratelimit-x");
import convert = require("koa-convert");
import * as Koa from "koa";
import {uniq_user} from "../common";
import index from "../core/config/index";
import * as RedisStorage from "ioredis";
// apply rate limit
export default async function (app: Koa) {
    let cfg = index.middlewares.ratelimit;
    let sid = index.middlewares.session.key;

    app.use(convert(ratelimit({
        ...cfg,
        ...{
            id: uniq_user.method(cfg.id, sid),
            db: new RedisStorage(cfg.db)
            // db: new RedisClient(cfg.db)
        }
    })));

    await Promise.resolve();
}