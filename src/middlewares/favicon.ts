import * as Koa from "koa";
import {Environment} from "../core/env";
import {join} from "path";
import favicon = require("koa-favicon");

export function set (app: Koa) {
    app.use(favicon(join(Environment.DIR.PUBLIC, "favicon.ico")));
    return Promise.resolve();
}
