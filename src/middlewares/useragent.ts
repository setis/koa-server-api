import userAgent from "koa2-useragent";
import * as Koa from "koa";
import {Middleware} from "koa";
import * as Router from "koa-router";

export function set(app: Koa | Router): Promise<void> {
    app.use(userAgent());
    return Promise.resolve();
}

export function list(): Promise<Middleware> {
    return Promise
        .resolve(
                userAgent()
        );
    }
