import * as cors from "koa2-cors";

import * as Koa from "koa";
import {Context, Middleware} from "koa";
import * as Router from "koa-router";

function configure(config: {
    origin: ((ctx: Context) => string) | string;
    allowMethods: string[];
    exposeHeaders: string[];
}): {
    origin: ((ctx: Context) => string) | string;
    allowMethods: string[];
    exposeHeaders: string[];
} {
    if (config.origin === "*") {
        config.origin = function (ctx: Context) {
            return ctx.request.headers["origin"];
        }
    } else if (config.origin instanceof Array) {
        let list = [...config.origin];
        config.origin = function (ctx: Context) {
            let origin = ctx.request.headers["origin"];
            if (list.indexOf(origin) !== -1) {
                return ctx.request.headers["origin"];
            }
        }
    }
    return config;
}

export function set(app: Koa | Router, config: {
    origin: ((ctx: Context) => string) | string;
    allowMethods: string[];
    exposeHeaders: string[];
}): Promise<void> {
    app.use(cors(configure(config)));
    return Promise.resolve();
}

export function list(config: {
    origin: ((ctx: Context) => string) | string;
    allowMethods: string[];
    exposeHeaders: string[];
}): Promise<Middleware> {
    return Promise.resolve(cors(configure(config)));
}