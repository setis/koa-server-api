import * as multer from "koa-multer";

import * as Koa from "koa";
import {Middleware} from "koa";
import * as Router from "koa-router";

export function set(app: Koa | Router, config: multer.Options): Promise<void> {
    app.use(multer(config).any());
    return Promise.resolve();
}

export function list(config: multer.Options): Promise<Middleware> {
    return Promise
        .resolve(
                multer(config).any()
        );
    }
